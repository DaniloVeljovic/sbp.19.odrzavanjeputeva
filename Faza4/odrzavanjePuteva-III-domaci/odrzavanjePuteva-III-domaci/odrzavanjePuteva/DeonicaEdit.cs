﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class DeonicaEdit : Form
    {
        public Deonica dBasic;
        public DeonicaEdit(Deonica db)
        {
            this.dBasic = db;
            InitializeComponent();
            PopulateData();
          
        }
        private void PopulateData()
        {
            txtOdKilometra.Text = dBasic.OdKilometra.ToString();
            txtDoKilometra.Text = dBasic.DoKilometra.ToString();
            txtOdGrada.Text = dBasic.OdGrada.ToString();
            txtDoGrada.Text = dBasic.DoGrada;
            dtpDatumOd.Value = dBasic.DatumOd;
            dtpDatumDo.Value = dBasic.DatumDo;

            List<Izvrsilac> iz = DTOManager.GetIzvrsilac();
            comboBox1.Items.Clear();
            foreach (Izvrsilac op in iz)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
                listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }
            Izvrsilac i = DTOManager.GetIzvrsilac(dBasic.Izvrsilac.IdRadnika);
            comboBox1.SelectedItem = i.Jmbg;

            comboBox1.Refresh();
            listBox1.Refresh();
        }

        private void DeonicaEdit_Load(object sender, EventArgs e)
        {

        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            dBasic.Izvrsilac = DTOManager.getIzvrsilacJMBG(comboBox1.SelectedItem.ToString());
            dBasic.OdGrada = txtOdGrada.Text;
            dBasic.DoGrada = txtDoGrada.Text;
            dBasic.OdKilometra = Convert.ToInt32(txtOdKilometra.Text);
            dBasic.DoKilometra = Convert.ToInt32(txtDoKilometra.Text);
            dBasic.DatumOd = dtpDatumOd.Value;
            dBasic.DatumDo = dtpDatumDo.Value;
            s.Update(dBasic);
            s.Flush();
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeonicaEdit_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                            this.DisplayRectangle);
        }
    }
}
