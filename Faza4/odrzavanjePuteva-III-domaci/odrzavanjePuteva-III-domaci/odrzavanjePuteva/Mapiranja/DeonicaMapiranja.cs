﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva.Mapiranja
{
    class DeonicaMapiranja : ClassMap<Deonica>
    {
        public DeonicaMapiranja()
        {

            //Mapiranje tabele
            Table("DEONICA");

            //mapiranje primarnog kljuca
            Id(x => x.IdDeonice, "IDDEONICE").GeneratedBy.TriggerIdentity().UnsavedValue(-1);   

            //mapiranje svojstava
            Map(x => x.DatumDo, "DATUMDO");
            Map(x => x.DatumOd, "DATUMOD");
            Map(x => x.DoGrada, "DOGRADA");
            Map(x => x.OdGrada, "ODGRADA");
            Map(x => x.DoKilometra, "DOKILOMETRA");
            Map(x => x.OdKilometra, "ODKILOMETRA");
            

            
            References(x => x.Gradiliste).Column("GRADILISTEID");

            References(x => x.Izvrsilac).Column("IDIZVRSIOCA");

            HasMany(x => x.AngazovanaVozila).KeyColumn("ID_DEONICE").LazyLoad().Cascade.All().Inverse(); //




        }
    }
}
