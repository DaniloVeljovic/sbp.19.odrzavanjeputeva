﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using NHibernate;


namespace odrzavanjePuteva
{
    public partial class VoziloEdit : Form
    {
        public Putnicka vBasic;
        public VoziloEdit()
        {
            InitializeComponent();
        }

        public VoziloEdit(int ib)
        {
            
            InitializeComponent();
            this.PopulateData(ib);
            
            
        }

        private void PopulateData(int ib)
        {
            ISession s1 = DataLayer.GetSession();
            Putnicka izv = s1.Get<Putnicka>(ib);

            txtRegNo.Text = izv.RegistarskaOznaka;
            txtTipG.Text = izv.TipGoriva;
            txtBoja.Text = izv.Boja;
            txtZapremina.Text = izv.ZapreminaMotora.ToString();
            txtBrojMesta.Text = izv.BrojMesta.ToString();

            listBox1.Items.Clear();
            List<Nadzornik> vInfos = DTOManager.GetNadzornici(izv.IdVozila);
            //List<Koriste> kInfos = GetKoriste(vBasic.RadniciKoriste);
            int i1 = 1;
            foreach (Koriste k in izv.RadniciKoriste)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});
                Nadzornik op = new Nadzornik();
                foreach (Nadzornik nadd in vInfos)
                {
                    if (k.Nadzornik.IdRadnika == nadd.IdRadnika)
                        op = nadd;
                }

                string s = $"Od datuma: {k.DatumOd} do datuma: {k.DatumDo} - {op.Ime} {op.Prezime} {op.Jmbg}";
                listBox1.Items.Add(s);
                i1++;
            }
            listBox1.Refresh();

            //this.osveziListBox();

            //napuni nadzornicima
            comboBox1.Items.Clear();
            List<Nadzornik> nad = DTOManager.GetNadzornici();

            //{op.Ime} {op.Prezime} 

            foreach (Nadzornik op in nad)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
            }
            comboBox1.Refresh();
            s1.Close();
            vBasic = izv;
        }

        private void osveziListBox()
        {

            listBox1.Items.Clear();
            List<Nadzornik> vInfos = DTOManager.GetNadzornici(vBasic.IdVozila);
            //List<Koriste> kInfos = GetKoriste(vBasic.RadniciKoriste);
            int i1 = 1;
            foreach (Koriste k in vBasic.RadniciKoriste)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});
                Nadzornik op = new Nadzornik();
                foreach (Nadzornik nadd in vInfos)
                {
                    if (k.Nadzornik.IdRadnika == nadd.IdRadnika)
                        op = nadd;
                }

                string s = $"Od datuma: {k.DatumOd} do datuma: {k.DatumDo} - {op.Ime} {op.Prezime} {op.Jmbg}";
                listBox1.Items.Add(s);
                i1++;
            }
            listBox1.Refresh();


            //s1.Close();
            /*listBox1.Items.Clear();
            List<Nadzornik> vInfos = DTOManager.GetNadzornici(vBasic.IdVozila);
            int i1 = 1;
            foreach (Nadzornik op in vInfos)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});

                listBox1.Items.Add($"{i1}) {op.Ime} {op.Prezime} {op.Jmbg}");
                i1++;
            }
            listBox1.Refresh();*/
        }

        private void VoziloEdit_Load(object sender, EventArgs e)
        {

            //PopulateData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Koriste k = new Koriste();
            k.DatumOd = datumOd.Value; //DateTime.Parse(textBox2.Text);
            k.DatumDo = datumDo.Value; //DateTime.Parse(textBox3.Text);
            k.Vozilo = vBasic;
            k.Nadzornik = DTOManager.GetNadzornik(comboBox1.SelectedItem.ToString());
            vBasic.RadniciKoriste.Add(k);
            ISession s = DataLayer.GetSession();
            s.Save(k);
            s.Flush();
            s.Update(vBasic);
            s.Flush();
            s.Close();
            this.osveziListBox();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            vBasic.Boja = txtBoja.Text;
            vBasic.BrojMesta =Convert.ToInt32( txtBrojMesta.Text);
            vBasic.RegistarskaOznaka = txtRegNo.Text;
            vBasic.TipGoriva = txtTipG.Text;
            vBasic.ZapreminaMotora = Convert.ToInt32( txtZapremina.Text);

            ISession s = DataLayer.GetSession();
            s.Update(vBasic);
            s.Flush();
            s.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void VoziloEdit_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                            this.DisplayRectangle);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
