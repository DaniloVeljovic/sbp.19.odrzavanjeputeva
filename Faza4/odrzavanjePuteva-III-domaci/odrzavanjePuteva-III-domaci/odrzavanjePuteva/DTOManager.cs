﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using NHibernate;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public class DTOManager
    {
        public static List<DeonicaPregled> GetDeonInfo()
        {
            List<DeonicaPregled> deoInfos = new List<DeonicaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Deonica> deonice = from o in s.Query<Deonica>()
                                               select o;

                foreach (Deonica o in deonice)
                {
                    deoInfos.Add(new DeonicaPregled(o.IdDeonice, o.OdKilometra, o.DoKilometra, o.OdGrada, o.DoGrada, o.Izvrsilac.IdRadnika, o.Gradiliste.IdGradilista,o.DatumOd));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return deoInfos;
        }

        public static void DeleteGradiliste(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static void DeleteIzvrsilac(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static void CreateGradiliste(Gradiliste gradiliste)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(gradiliste);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {

            }
        }
        public  int CreateGradilisteInt(GradilistePregled gradiliste)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(gradiliste);
                s.Flush();
                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
                return -1;
            }
            
        }
        public static void CreateGradilistePregled(GradilistePregled gradiliste)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(gradiliste);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.ToString());
            }
        }
        public static bool DodajGradiliste(String tip)
        {
            Gradiliste gradiliste = new Gradiliste();
            gradiliste.TipGradilista = tip;

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(gradiliste);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                //TODO handle
                return false;
            }
            return true;
        }
        public static GradilisteBasic UpdateGradilisteBasic(GradilisteBasic gb)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(gb.IdGradilista);
                o.TipGradilista = gb.TipGradilista;


                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return gb;
        }
        public static int  UpdateGradilistePregled(GradilistePregled gb)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(gb.IdGradilista);
                o.TipGradilista = gb.TipGradilista;


                s.Update(o);
                s.Flush();

                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                //handle exceptions return -1;
                return -1;
            }
            

        }

        internal static GradilisteBasic GetGradilisteBasic(int gradilisteId)
        {
            GradilisteBasic db = new GradilisteBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(gradilisteId);
                db = new GradilisteBasic(o.IdGradilista, o.TipGradilista);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        internal static List<Izvrsilac> GetIzvrsilac()
        {
            List<Izvrsilac> nad = new List<Izvrsilac>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> izv = from o in s.Query<Izvrsilac>()
                                             select o;

                foreach (Izvrsilac o in izv)
                {
                    nad.Add(o);
                }

                s.Close();
            }
            catch (Exception e)
            {

            }

            return nad;
        }

        internal static Izvrsilac getIzvrsilacJMBG(string v)
        {
            Izvrsilac vozInfos = new Izvrsilac();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> vozila = from o in s.Query<Izvrsilac>()
                                                where o.Jmbg == v
                                                select o;

                foreach (Izvrsilac vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        internal static Izvrsilac GetIzvrsilac(int idRadnika)
        {
            ISession s = DataLayer.GetSession();
            Izvrsilac ret = s.Get<Izvrsilac>(idRadnika);
            s.Close();
            return ret;
        }

        public static bool DodajIzvrsioca(String gr,String a,String jmbg, String ime,String p,String oi)
        {
            Izvrsilac iz = new Izvrsilac();
            iz.GodinaRodjenja = gr;
            iz.Ime = ime;
            iz.Prezime = p;
            iz.Adresa = a;
            iz.Jmbg = jmbg;
            iz.OcevoIme = oi;

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(iz);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                //TODO handle
                return false;
            }
            return true;

        }
      /*  public static bool DodajDeonicu(DeonicaPregledCeli bankomat)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();

                Deonica deonicaZaBazu = new Deonica();
                // SetujPropertijeBankomata(bankomatZaBazu, bankomat);
                deonicaZaBazu.DatumOd = bankomat.DatumOd;
                deonicaZaBazu.DoGrada = bankomat.DoGrada;
                deonicaZaBazu.OdGrada = bankomat.OdGrada;
                deonicaZaBazu.OdKilometra = bankomat.OdKilometra;
                deonicaZaBazu.DoKilometra = bankomat.DoKilometra;
                
                Gradiliste gradiliste = sesija.Load<Gradiliste>(bankomat.IdGradiliste);
                deonicaZaBazu.Gradiliste = gradiliste;
                gradiliste.Deonice.Add(deonicaZaBazu);

                sesija.SaveOrUpdate(deonicaZaBazu);
                sesija.SaveOrUpdate(gradiliste);

                sesija.Flush();
                sesija.Close();
            }
            catch (Exception ec)
            {
                return false;
            }
            return true;
        }*/
        public static List<DeonicaPregled> GetDeonicaInfo(Izvrsilac id)
        {
            List<DeonicaPregled> deoInfos = new List<DeonicaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Deonica> deonice = from o in s.Query<Deonica>()
                                               where o.Izvrsilac == id
                                               select o;

                foreach (Deonica o in deonice)
                {
                    deoInfos.Add(new DeonicaPregled(o.IdDeonice, o.DoKilometra, o.DoKilometra, o.OdGrada, o.DoGrada,o.Izvrsilac.IdRadnika,o.Gradiliste.IdGradilista,o.DatumOd));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return deoInfos;
        }
       
        internal static List<Nadzornik> GetNadzornici(int idVozila)
        {
            List<Nadzornik> vozInfos = new List<Nadzornik>();
            try
            {
                ISession s = DataLayer.GetSession();

                Vozilo id = s.Load<Vozilo>(idVozila);

                foreach (Koriste up in id.RadniciKoriste)
                {
                    IEnumerable<Nadzornik> vozila = from o in s.Query<Nadzornik>()
                                                 where up.Nadzornik == o
                                                 select o;

                    foreach (Nadzornik o in vozila)
                    {

                        vozInfos.Add(o);
                    }
                }


                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Nadzornik GetNadzornik(string v)
        {
            Nadzornik vozInfos = new Nadzornik();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Nadzornik> vozila = from o in s.Query<Nadzornik>()
                                             where o.Jmbg == v
                                             select o;

                foreach (Nadzornik vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Deonica GetDeonica(int dId)
        {
            ISession s = DataLayer.GetSession();
            Deonica ret = s.Get<Deonica>(dId);
            s.Close();
            return ret;
        }

        public static List<Nadzornik> GetNadzornici()
        {
            List<Nadzornik> nad = new List<Nadzornik>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Nadzornik> izv = from o in s.Query<Nadzornik>()
                                             select o;

                foreach (Nadzornik o in izv)
                {
                    nad.Add(o);
                }

                s.Close();
            }
            catch (Exception e)
            {

            }

            return nad;
        }

        public static DeonicaBasic GetDeonicaBasic(int dId)
        {
            DeonicaBasic db = new DeonicaBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(dId);
                db = new DeonicaBasic(o.IdDeonice, o.DoKilometra, o.OdGrada, o.DoGrada);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        public static DeonicaBasic UpdateDeonicaBasic(DeonicaBasic db)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(db.deonicaId);
                o.DoKilometra = db.DoKilometra;
                o.OdGrada = db.OdGrada;
                o.DoGrada = db.DoGrada;

                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return db;
        }

        public static List<PutnickaPregled> GetVozInfo()
        {
            List<PutnickaPregled> vozInfos = new List<PutnickaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Putnicka> vozila = from o in s.Query<Putnicka>()
                                               select o;

                foreach (Vozilo o in vozila)
                {
                    vozInfos.Add(new PutnickaPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora, o.BrojMesta));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Vozilo GetVoziloRegNo(string v)
        {
            Vozilo vozInfos = new Vozilo();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                             where o.RegistarskaOznaka == v
                                             select o;

                foreach(Vozilo vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

       

        public static List<GradilistePregled> GetGradnfo()
        {
            List<GradilistePregled> gradInfos = new List<GradilistePregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Gradiliste> gradiliste = from o in s.Query<Gradiliste>()
                                                     select o;

                foreach (Gradiliste o in gradiliste)
                {
                    gradInfos.Add(new GradilistePregled(o.IdGradilista, o.TipGradilista));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return gradInfos;
        }
        public static GradilistePregled GetGradilisteId(int id)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();


                Gradiliste gradiliste = sesija.Load<Gradiliste>(id);
                GradilistePregled gradilisteRet = new GradilistePregled(gradiliste.IdGradilista, gradiliste.TipGradilista);
                sesija.Close();
                return gradilisteRet;
            }
            catch (Exception ec)
            {
                return null;
            }
        }
        public static List<IzvrsilacPregled> GetIzvrsilacInfo()
        {
            List<IzvrsilacPregled> izvInfos = new List<IzvrsilacPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> izv = from o in s.Query<Izvrsilac>()
                                             select o;

                foreach (Izvrsilac o in izv)
                {
                    izvInfos.Add(new IzvrsilacPregled(o.IdRadnika, o.GodinaRodjenja, o.Adresa, o.Jmbg, o.Ime, o.Prezime,o.OcevoIme));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return izvInfos;
        }
        public static IzvrsilacPregled GetIzvrsilacPregledId(int id)
        {

            try
            {
                ISession sesija = DataLayer.GetSession();


                Izvrsilac izv = sesija.Load<Izvrsilac>(id);
                IzvrsilacPregled izvRet = new IzvrsilacPregled(izv.IdRadnika,izv.GodinaRodjenja,izv.Adresa,izv.Jmbg,izv.Ime,izv.Prezime,izv.OcevoIme);
                sesija.Close();
                return izvRet;
            }
            catch (Exception ec)
            {
                return null;
            }
        }

        public static IzvrsilacBasic GetIzvrsilacBasic(int dId)
        {
            IzvrsilacBasic db = new IzvrsilacBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(dId);
                db = new IzvrsilacBasic(o.IdRadnika, o.OcevoIme, o.MatBrSefa, o.Ime, o.Prezime);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }
        public static DeonicaPregled GetDeonicaPregled(int dId)
        {
            DeonicaPregled db = new DeonicaPregled();
            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(dId);
                db = new DeonicaPregled(o.IdDeonice, o.OdKilometra, o.DoKilometra, o.OdGrada, o.DoGrada, o.Izvrsilac.IdRadnika, o.Gradiliste.IdGradilista,o.DatumOd);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        public static bool DodajSaradnika(int brU)
        {
            SpoljniSaradnik ss = new SpoljniSaradnik();
            ss.BrojUgovoraODelu = brU;
            Radnik nadzornik = new Radnik();


            try
            {
                ISession s = DataLayer.GetSession();
                nadzornik = s.Load<Nadzornik>(20);
                ss.Nadzornik = nadzornik;
                s.SaveOrUpdate(ss);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                //TODO handle
                return false;
            }
            return true;
        }



       public static bool DodajDeonicu(Deonica deonica)
        {

            try
            {
                ISession s = DataLayer.GetSession();
                Deonica deonicaZaBazu = new Deonica();
                deonicaZaBazu.OdKilometra = deonica.OdKilometra;
                deonicaZaBazu.DoKilometra = deonica.DoKilometra;
                deonicaZaBazu.OdGrada = deonica.OdGrada;
                deonicaZaBazu.DoGrada = deonica.DoGrada;
                deonicaZaBazu.DatumOd = deonica.DatumOd;
              //  deonicaZaBazu.DatumDo = deonica.DatumDo;


                Gradiliste gradiliste = s.Load<Gradiliste>(deonica.Gradiliste.IdGradilista);
                Radnik izvrslic = s.Load<Radnik>(deonica.Izvrsilac.IdRadnika);

            
                deonicaZaBazu.Izvrsilac= izvrslic;
                deonicaZaBazu.Gradiliste = gradiliste;
                 s.Save(deonicaZaBazu);
                s.Flush();
                s.Close();
                return true;
            }
            catch (Exception ec)
            {
                return false;
            }

        }

      
        
        public static int UpdateDeonicaPregled(DeonicaPregled db)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(db.IdDeonice);
                o.OdKilometra = db.OdKilometra;
                o.DoKilometra = db.DoKilometra;
                o.OdGrada = db.OdGrada;
                o.DoGrada = db.DoGrada;
                o.DatumOd = db.DatumOd;

                s.Update(o);
                s.Flush();

                s.Close();
                return 1;

            }
            catch (Exception ec)
            {
                //handle exceptions
                return -1;
            }
        }
        public static void DeleteDeonicu(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Deonica d = s.Load<Deonica>(id);

                s.Delete(d);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }
        

        public static IzvrsilacBasic UpdateIzvrsilacBasic(IzvrsilacBasic db)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(db.IdRadnika);
                o.Ime = db.Ime;
                o.MatBrSefa = db.MatBrSefa;
                o.OcevoIme = db.OcevoIme;
                o.Prezime = db.Prezime;


                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return db;
        }
        public static int UpdateIzvrsilacPregled(IzvrsilacPregled ip)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(ip.IdRadnika);
               
                o.GodinaRodjenja = ip.GodinaRodjenja;
                o.Adresa = ip.Adresa;
                o.Ime = ip.Ime;
                o.Prezime = ip.Prezime;
                o.Jmbg = o.Jmbg;


                s.Update(o);
                s.Flush();

                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                //handle exceptions return -1;
                return -1;
            }
        }


        public static List<SpoljniSaradnikPregled> GetSpoljniSInfo()
        {
            List<SpoljniSaradnikPregled> spoljInfos = new List<SpoljniSaradnikPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<SpoljniSaradnik> saradnici = from o in s.Query<SpoljniSaradnik>()
                                                         select o;

                foreach (SpoljniSaradnik o in saradnici)
                { 
                    spoljInfos.Add(new SpoljniSaradnikPregled(o.IdSpoljnogSaradnika, o.BrojUgovoraODelu,o.Nadzornik.IdRadnika));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return spoljInfos;
        }

        public static void DeleteSpoljniSaradnikPregled(SpoljniSaradnikPregled ss)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(ss.IdSpoljnogSaradnika);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static SpoljniSaradnikPregled GetSpoljni(int dId)
        {
            SpoljniSaradnikPregled db = new SpoljniSaradnikPregled();
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Get<SpoljniSaradnik>(dId);
                db = new SpoljniSaradnikPregled(o.IdSpoljnogSaradnika, o.BrojUgovoraODelu);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }
        public static SpoljniSaradnikPregled GetSaradnikId(int id)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();


                SpoljniSaradnik ss = sesija.Load<SpoljniSaradnik>(id);
                SpoljniSaradnikPregled saradnikRet = new SpoljniSaradnikPregled(ss.IdSpoljnogSaradnika,ss.BrojUgovoraODelu,ss.Nadzornik.IdRadnika);
                sesija.Close();
                return saradnikRet;
            }
            catch (Exception ec)
            {
                return null;
            }
        }

        public static void CreateIzvrsilac(Izvrsilac izvrsilac)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(izvrsilac);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                
            }
        }

        public static void DeleteIzvrsilacPregled(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static List<Vozilo> GetVozilaInfo(int id1)
        {
            List<Vozilo> vozInfos = new List<Vozilo>();
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac id = s.Load<Izvrsilac>(id1);

                foreach (Upravlja up in id.UpravljaVozilima)
                {
                    IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                                 where up.Vozilo == o
                                                 select o;

                    foreach (Vozilo o in vozila)
                    {
                        vozInfos.Add(o);
                        //vozInfos.Add(new VoziloPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora));
                    }
                }
                

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static List<VoziloPregled> GetVozilaInfo()
        {
            List<VoziloPregled> vozInfos = new List<VoziloPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                               select o;

                foreach (Vozilo o in vozila)
                {
                    vozInfos.Add(new VoziloPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }
        ////////////////////////////////////////////////
        public static int UpdateSpoljniSaradnikPregled(SpoljniSaradnikPregled ssp)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(ssp.IdSpoljnogSaradnika);
                o.BrojUgovoraODelu = ssp.BrojUgovoraODelu;


                s.Update(o);
                s.Flush();

                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                //handle exceptions return -1;
                return -1;
            }


        }
        public static void DeleteSpoljniSaradnikPregled(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(id);

                s.Delete(o);
                s.Flush();

                s.Close();




            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }
        public static List<NadzornikPregled> GetNadzornikInfo()
        {
            List<NadzornikPregled> nadInfos = new List<NadzornikPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Nadzornik> nadzornik = from o in s.Query<Nadzornik>()
                                             select o;

                foreach (Nadzornik o in nadzornik)
                {
                    nadInfos.Add(new NadzornikPregled(o.IdRadnika, o.GodinaRodjenja, o.Adresa, o.Jmbg, o.Ime, o.Prezime, o.OcevoIme));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return nadInfos;
        }
        public static NadzornikPregled GetNadzornikPregledId(int id)
        {

            try
            {
                ISession sesija = DataLayer.GetSession();


                Nadzornik izv = sesija.Load<Nadzornik>(id);
                NadzornikPregled izvRet = new NadzornikPregled(izv.IdRadnika, izv.GodinaRodjenja, izv.Adresa, izv.Jmbg, izv.Ime, izv.Prezime, izv.OcevoIme);
                sesija.Close();
                return izvRet;
            }
            catch (Exception ec)
            {
                return null;
            }
        }
        public static bool DodajNadzornik(String gr, String a, String jmbg, String ime, String p, String oi)
        {
            Nadzornik iz = new Nadzornik();
            iz.GodinaRodjenja = gr;
            iz.Ime = ime;
            iz.Prezime = p;
            iz.Adresa = a;
            iz.Jmbg = jmbg;
            iz.OcevoIme = oi;

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(iz);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                //TODO handle
                return false;
            }
            return true;

        }
        public static int UpdateNadzornikPregled(NadzornikPregled ip)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Nadzornik o = s.Load<Nadzornik>(ip.IdRadnika);

                o.GodinaRodjenja = ip.GodinaRodjenja;
                o.Adresa = ip.Adresa;
                o.Ime = ip.Ime;
                o.Prezime = ip.Prezime;
                o.Jmbg = o.Jmbg;


                s.Update(o);
                s.Flush();

                s.Close();
                return 1;
            }
            catch (Exception ec)
            {
                //handle exceptions return -1;
                return -1;
            }
        }
        public static void DeleteNadzornikPregled(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Nadzornik o = s.Load<Nadzornik>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }
        public static DeonicaPregledCeli UcitajInformacijeDeonicaPoId(int id)
        {
            try
            {
              
                ISession sesija = DataLayer.GetSession();


                Deonica deonica = sesija.Load<Deonica>(id);
                DeonicaPregledCeli deonicaRet = new DeonicaPregledCeli(deonica.IdDeonice, deonica.OdKilometra, deonica.DoKilometra, deonica.OdGrada, deonica.DoGrada,deonica.Gradiliste.IdGradilista,  deonica.Izvrsilac.IdRadnika);
                sesija.Close();
                return deonicaRet;
            }
            catch (Exception ec)
            {
                return null;
              
            }

        }
        /////////////////////T

        public static void DeleteVozilo(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Vozilo o = s.Load<Vozilo>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static VoziloPregled GetVoziloId(int id)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();


                Vozilo vozilo = sesija.Load<Vozilo>(id);
                VoziloPregled voziloP = new VoziloPregled(vozilo.IdVozila, vozilo.RegistarskaOznaka, vozilo.TipGoriva, vozilo.Boja, vozilo.ZapreminaMotora);
                sesija.Close();
                return voziloP;
            }
            catch (Exception ec)
            {
                return null;
            }
        }

        public static List<Vozilo> GetVoziloInfo()
        {
            List<Vozilo> voziloInfo = new List<Vozilo>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Vozilo> vozilo = from o in s.Query<Vozilo>()
                                             select o;

                foreach (Vozilo o in vozilo)
                {
                    voziloInfo.Add(new Vozilo(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora, o.TipVozila, o.BrojMesta, o.Nosivost, o.BrojOsovina, o.TipRadneMasine, o.TipPogonaRadneMasine));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return voziloInfo;
        }


        public static bool CreatePutnicka(PutnickaPregled p)
        {
            Putnicka putnicko = new Putnicka(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.BrojMesta);

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(putnicko);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {

                return false;
            }
            return true;
        }


        public static List<PutnickaPregled> LoadPutnicka()
        {
            List<PutnickaPregled> lista = new List<PutnickaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();
                IQuery q = s.CreateQuery("from Putnicka order by IdVozila asc");

                IList<Putnicka> vozila = q.List<Putnicka>();


                foreach (Putnicka p in vozila)
                {
                    lista.Add(new PutnickaPregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.BrojMesta));
                }

                s.Close();


            }
            catch (Exception ec)
            {
                //handle exceptions
            }
            return lista;
        }

        public static PutnickaPregled LoadPutnickaId(int id)
        {
            PutnickaPregled putnickaPregled = null;
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Putnicka where IdVozila = " + id.ToString() + " order by IdVozila asc");

                Putnicka p = q.UniqueResult<Putnicka>();


                putnickaPregled = new PutnickaPregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.BrojMesta);


                s.Close();

            }
            catch (Exception ec)
            {

            }
            return putnickaPregled;
        }

        public static bool DeletePutnicka(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Putnicka o = s.Load<Putnicka>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

        public static bool UpdatePutnicka(PutnickaPregled vozilo)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();
                Putnicka ucitanoVozilo = sesija.Load<Putnicka>(vozilo.IdVozila);


                ucitanoVozilo.BrojMesta = vozilo.BrojMesta;
                ucitanoVozilo.TipGoriva = vozilo.TipGoriva;
                ucitanoVozilo.RegistarskaOznaka = vozilo.RegistarskaOznaka;
                ucitanoVozilo.Boja = vozilo.Boja;
                ucitanoVozilo.ZapreminaMotora = vozilo.ZapreminaMotora;


                sesija.Update(ucitanoVozilo);
                sesija.Flush();
                sesija.Close();
                return true;
            }
            catch (Exception ec)
            {

                return false;
            }
        }
        public static bool CreateTeretna(TeretnaPregled p)
        {
            Teretne teretne = new Teretne(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.Nosivost, p.BrojOsovina);

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(teretne);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {

                return false;
            }
            return true;
        }


        public static List<TeretnaPregled> LoadTeretna()
        {
            List<TeretnaPregled> lista = new List<TeretnaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Teretne order by IdVozila asc");

                IList<Teretne> vozila = q.List<Teretne>();


                foreach (Teretne p in vozila)
                {
                    lista.Add(new TeretnaPregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.Nosivost, p.BrojOsovina));
                }

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
            return lista;
        }

        public static TeretnaPregled LoadTeretnaId(int id)
        {
            TeretnaPregled teretnaPregled = null;
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from Teretne where IdVozila = " + id.ToString() + " order by IdVozila asc");

                Teretne p = q.UniqueResult<Teretne>();


                teretnaPregled = new TeretnaPregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.Nosivost, p.BrojOsovina);


                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
            return teretnaPregled;
        }

        public static bool DeleteTeretna(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Teretne o = s.Load<Teretne>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

        public static bool UpdateTeretna(TeretnaPregled vozilo)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();
                Teretne ucitanoVozilo = sesija.Load<Teretne>(vozilo.IdVozila);


                ucitanoVozilo.BrojOsovina = vozilo.BrojOsovina;
                ucitanoVozilo.Nosivost = vozilo.Nosivost;
                ucitanoVozilo.TipGoriva = vozilo.TipGoriva;
                ucitanoVozilo.RegistarskaOznaka = vozilo.RegistarskaOznaka;
                ucitanoVozilo.Boja = vozilo.Boja;
                ucitanoVozilo.ZapreminaMotora = vozilo.ZapreminaMotora;


                sesija.Update(ucitanoVozilo);
                sesija.Flush();
                sesija.Close();
                return true;
            }
            catch (Exception ec)
            {

                return false;
            }
        }
        public static bool CreateRadneMasine(RadneMasinePregled p)
        {
            RadneMasine masine = new RadneMasine(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.TipRadneMasine, p.TipPogonaRadneMasine);

            try
            {
                ISession s = DataLayer.GetSession();
                s.SaveOrUpdate(masine);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                //TODO handle
                return false;
            }
            return true;
        }


        public static List<RadneMasinePregled> LoadRadneMasine()
        {
            List<RadneMasinePregled> lista = new List<RadneMasinePregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IQuery q = s.CreateQuery("from RadneMasine order by IdVozila asc");

                IList<RadneMasine> vozila = q.List<RadneMasine>();


                foreach (RadneMasine p in vozila)
                {
                    lista.Add(new RadneMasinePregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.TipRadneMasine, p.TipPogonaRadneMasine));
                }

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
            return lista;
        }

        public static RadneMasinePregled LoadRadneMasineId(int id)
        {
            RadneMasinePregled masinePregled = null;
            try
            {
                ISession s = DataLayer.GetSession();
                IQuery q = s.CreateQuery("from RadneMasine where IdVozila = " + id.ToString() + " order by IdVozila asc");

                RadneMasine p = q.UniqueResult<RadneMasine>();


                masinePregled = new RadneMasinePregled(p.IdVozila, p.RegistarskaOznaka, p.TipGoriva, p.Boja, p.ZapreminaMotora, p.TipRadneMasine, p.TipPogonaRadneMasine);


                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
            return masinePregled;
        }

        public static bool DeleteRadneMasine(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                RadneMasine o = s.Load<RadneMasine>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

        public static bool UpdateRadneMasine(RadneMasinePregled vozilo)
        {
            try
            {
                ISession sesija = DataLayer.GetSession();
                RadneMasine ucitanoVozilo = sesija.Load<RadneMasine>(vozilo.IdVozila);


                ucitanoVozilo.TipRadneMasine = vozilo.TipRadneMasine;
                ucitanoVozilo.TipPogonaRadneMasine = vozilo.TipPogonaRadneMasine;
                ucitanoVozilo.TipGoriva = vozilo.TipGoriva;
                ucitanoVozilo.RegistarskaOznaka = vozilo.RegistarskaOznaka;
                ucitanoVozilo.Boja = vozilo.Boja;
                ucitanoVozilo.ZapreminaMotora = vozilo.ZapreminaMotora;


                sesija.Update(ucitanoVozilo);
                sesija.Flush();
                sesija.Close();
                return true;
            }
            catch (Exception ec)
            {

                return false;
            }
        }
    }
}
