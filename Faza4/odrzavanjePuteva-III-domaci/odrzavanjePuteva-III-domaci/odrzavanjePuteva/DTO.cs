﻿using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva
{
    public class GradilistePregled
    {
        public int IdGradilista { get;  set; }
        public String TipGradilista { get; set; }

        public GradilistePregled(int id, string tg)
        {
            this.IdGradilista = id;
            this.TipGradilista = tg;
        }
        public GradilistePregled()
        {

        }
    }
    public class GradilisteBasic
    {
        public int IdGradilista { get; protected set; }
        public String TipGradilista { get; set; }

        public GradilisteBasic(int id, string tg)
        {
            this.IdGradilista = id;
            this.TipGradilista = tg;
        }
        public GradilisteBasic()
        {

        }
    }

    public class DeonicaPregled
    {
        public int IdDeonice { get; set; }
        public int OdKilometra { get; set; }
        public int DoKilometra { get; set; }
        public string OdGrada { get; set; }
        public string DoGrada { get; set; }
        
        public int IdIzvrsioca { get; set; }
        public int IdGradiliste { get; set; }
        public DateTime DatumOd { get; set; }
     


        public DeonicaPregled(int id, int ok, int dk, string og, string dg,int idI,int idG,DateTime datumod)
        {
            this.IdDeonice = id;
            this.OdKilometra = ok;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
            this.IdIzvrsioca = idI;
            this.IdGradiliste = idG;
            this.DatumOd = datumod;
        }
        public DeonicaPregled()
        {

        }
    }
    public class DeonicaBasic
    {
        public int deonicaId { get; set; }
        public int DoKilometra { get; set; }
        public int OdKilometra { get; set; }  //ovo sam dodala
        public DateTime DatumOd { get; set; }//ovo sam dodala
        public DateTime DatumDo { get; set; } //ovo sam dodala
        public virtual String OdGrada { get; set; }
        public virtual String DoGrada { get; set; }

        public DeonicaBasic()
        {

        }

        public DeonicaBasic(int id, int dk, string og, string dg)
        {
            this.deonicaId = id;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
        }
    }

    public class PutnickaPregled
    {
        public int IdVozila { get;  set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }
        public int BrojMesta { get; set; }

        public PutnickaPregled(int id, string ro, string tg, string b, int zm, int bm)
        {
            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;
            this.BrojMesta = bm;
        }
        public PutnickaPregled()
        {

        }
    }

    public class VoziloPregled
    {
        public int IdVozila { get; protected set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }


        public VoziloPregled(int id, string ro, string tg, string b, int zm)
        {
            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;

        }
    }
    public class TeretnaPregled
    {
        public int IdVozila { get; set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }
        public int Nosivost { get; set; }
        public int BrojOsovina { get; set; }

        public TeretnaPregled(int id, string ro, string tg, string b, int zm, int nosivost, int brojosovina)
        {
            

            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;
            this.Nosivost = nosivost;
            this.BrojOsovina = brojosovina;
        }
        public TeretnaPregled()
        {

        }
    }
    public class RadneMasinePregled
    {
        public int IdVozila { get; set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }
        public String TipRadneMasine { get; set; }
        public string TipPogonaRadneMasine { get; set; }

        public RadneMasinePregled(int id, string ro, string tg, string b, int zm, string tipradnemasine, string tippogona)
        {
           


            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;
            this.TipRadneMasine = tipradnemasine;
            this.TipPogonaRadneMasine = tippogona;
        }
        public RadneMasinePregled()
        {

        }
    }
    public class IzvrsilacPregled
    {
        public int IdRadnika { get;  set; }
        public String GodinaRodjenja { get; set; }
        public String Adresa { get; set; }
        public String Jmbg { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public String OcevoIme { get; set; }

        public IzvrsilacPregled(int id, String gr, String a, String j, String i, String p,String oi)
        {
            this.IdRadnika = id;
            this.GodinaRodjenja = gr;
            this.OcevoIme = oi;
            this.Adresa = a;
            this.Jmbg = j;
            this.Ime = i;
            this.Prezime = p;
        }
        public IzvrsilacPregled()
        {

        }
    }
    public class NadzornikPregled
    {
        public int IdRadnika { get; set; }
        public String GodinaRodjenja { get; set; }
        public String Adresa { get; set; }
        public String Jmbg { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }
        public String OcevoIme { get; set; }
       

        public NadzornikPregled(int id, String gr, String a, String j, String i, String p, String oi)
        {
            this.IdRadnika = id;
            this.GodinaRodjenja = gr;
            this.OcevoIme = oi;
            this.Adresa = a;
            this.Jmbg = j;
            this.Ime = i;
            this.Prezime = p;
        }
        public NadzornikPregled()
        {

        }
    }
    public class IzvrsilacBasic
    {
        public int IdRadnika { get; protected set; }
        public String OcevoIme { get; set; }
        public String MatBrSefa { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }

        public IzvrsilacBasic()
        {
        }

        public IzvrsilacBasic(int id, String o, String mbs, String i, String p)
        {
            this.IdRadnika = id;
            this.OcevoIme = o;
            this.MatBrSefa = mbs;
            this.Ime = i;
            this.Prezime = p;
        }
    }

    public class SpoljniSaradnikPregled
    {
        public int IdSpoljnogSaradnika { get; set; }
        public int BrojUgovoraODelu { get; set; }
        public virtual int Nadzornik { get; set; }


        public SpoljniSaradnikPregled()
        {
      
        }

        public SpoljniSaradnikPregled(int id, int buod)
        {
            this.IdSpoljnogSaradnika = id;
            this.BrojUgovoraODelu = buod;
           
        }
        public SpoljniSaradnikPregled(int id, int buod,int idN)
        {
            this.IdSpoljnogSaradnika = id;
            this.BrojUgovoraODelu = buod;
            this.Nadzornik = idN;

        }
    }
    public class DeonicaPregledCeli
    {
       
        public int IdDeonice { get; set; }
        public int OdKilometra { get; set; }
        public int DoKilometra { get; set; }
        public string OdGrada { get; set; }
        public string DoGrada { get; set; }
       
        public int IdIzvrsioca { get; set; }
        public int IdGradiliste { get; set; }
        public DateTime DatumOd { get; set; }

        public DeonicaPregledCeli(int id, int ok, int dk, string og, string dg,int idgradilista,int idizvrsioca)
        {
            this.IdDeonice = id;
            this.OdKilometra = ok;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
            this.IdGradiliste = idgradilista;
            this.IdIzvrsioca = idizvrsioca;
        }
        public DeonicaPregledCeli()
        {

        }
        public DeonicaPregledCeli( int ok, int dk, string og, string dg, int idgradilista, int idizvrsioca)
        {
            
            this.OdKilometra = ok;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
            this.IdGradiliste = idgradilista;
            this.IdIzvrsioca = idizvrsioca;
        }
    }
}
