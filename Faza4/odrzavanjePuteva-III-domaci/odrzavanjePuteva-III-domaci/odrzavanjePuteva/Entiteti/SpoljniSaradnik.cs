﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva.Entiteti
{
    public class SpoljniSaradnik
    {
        public virtual int IdSpoljnogSaradnika { get; set; }
        public virtual int BrojUgovoraODelu { get; set; }
        public virtual Radnik Nadzornik { get; set; }
        public virtual String Ime { get; set; }
        public virtual String Prezime { get; set; }
    }
   
}
