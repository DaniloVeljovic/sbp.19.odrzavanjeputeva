﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class SpoljniSaradnikDodaj : Form
    {
        public SpoljniSaradnikDodaj()
        {
            InitializeComponent();
        }

        private void SpoljniSaradnikDodaj_Load(object sender, EventArgs e)
        {
            comboBox1.Items.Clear();
            List<Nadzornik> nad = DTOManager.GetNadzornici();

            //{op.Ime} {op.Prezime} 

            foreach (Nadzornik op in nad)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
                listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }
            comboBox1.Refresh();
            listBox1.Refresh();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            SpoljniSaradnik ss = new SpoljniSaradnik();
            ss.BrojUgovoraODelu = Convert.ToInt32(textBox1.Text);
            ss.Nadzornik = DTOManager.GetNadzornik(comboBox1.SelectedItem.ToString());
            s.Save(ss);
            s.Close();
            this.Close();
            this.DialogResult = DialogResult.OK;
        }

        private void SpoljniSaradnikDodaj_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 5),
                            this.DisplayRectangle);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
