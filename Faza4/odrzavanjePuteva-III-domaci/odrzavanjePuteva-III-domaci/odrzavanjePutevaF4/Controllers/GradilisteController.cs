﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Text;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;
namespace odrzavanjePutevaF4.Controllers
{
    public class GradilisteController : ApiController
    {
        public IList<GradilistePregled> Get()
        {
            DTOManager dto = new DTOManager();
            //IEnumerable<GradilistePregled> gradilista = DTOManager.GetGradnfo();
            IList<GradilistePregled> gradilista = DTOManager.GetGradnfo();
           
            return gradilista;
        }
        public GradilistePregled Get(int id)
        {
            GradilistePregled gp = DTOManager.GetGradilisteId(id);
            return gp;
        }

        public int Post([FromBody]GradilistePregled gradiliste)
        {
            if (DTOManager.DodajGradiliste(gradiliste.TipGradilista))
            {
                return 1;
            }
            else
                return -1;

        }

        public int Put(int id, [FromBody]GradilistePregled gradilsite)
        {
            gradilsite.IdGradilista = id;
            return DTOManager.UpdateGradilistePregled(gradilsite);

        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteGradiliste(id);
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}