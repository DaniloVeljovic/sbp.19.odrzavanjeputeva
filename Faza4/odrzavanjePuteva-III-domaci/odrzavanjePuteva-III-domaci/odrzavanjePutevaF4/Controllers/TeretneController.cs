﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class TeretneController : ApiController
    {

        public IEnumerable<TeretnaPregled> Get()
        {
            IEnumerable<TeretnaPregled> teretna = DTOManager.LoadTeretna();
            return teretna;
        }


        public TeretnaPregled Get(int id)
        {
            TeretnaPregled vozilo = DTOManager.LoadTeretnaId(id);
            return vozilo;
        }


        public int Post([FromBody]TeretnaPregled vozilo)
        {
            if (DTOManager.CreateTeretna(vozilo))
                return 1;
            return -1;
        }

        public int Put(int id, [FromBody]TeretnaPregled vozilo)
        {
            vozilo.IdVozila = id;
            if (DTOManager.UpdateTeretna(vozilo))
                return 1;
            return -1;
        }

        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteTeretna(id);
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
    }
}
