﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;

using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class RadneMasineController : ApiController
    {
        
            public IEnumerable<RadneMasinePregled> Get()
            {
                IEnumerable<RadneMasinePregled> masina = DTOManager.LoadRadneMasine();
                return masina;
            }


            public RadneMasinePregled Get(int id)
            {
                RadneMasinePregled vozilo = DTOManager.LoadRadneMasineId(id);
                return vozilo;
            }


            public int Post([FromBody]RadneMasinePregled vozilo)
            {
                if (DTOManager.CreateRadneMasine(vozilo))
                    return 1;
                return -1;
            }

            public int Put(int id, [FromBody]RadneMasinePregled vozilo)
            {
                vozilo.IdVozila = id;
                if (DTOManager.UpdateRadneMasine(vozilo))
                    return 1;
                return -1;
            }

            public int Delete(int id)
            {
                try
                {
                    DTOManager.DeleteRadneMasine(id);
                    return 1;
                }
                catch (Exception e)
                {
                    return -1;
                }
            }
        }
}
