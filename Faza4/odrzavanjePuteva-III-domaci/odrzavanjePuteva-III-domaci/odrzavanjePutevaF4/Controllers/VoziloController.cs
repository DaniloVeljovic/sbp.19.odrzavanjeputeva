﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class VoziloController : ApiController
    {
        public IEnumerable<Vozilo> Get()
        {
            DTOManager dto = new DTOManager();
            //IEnumerable<GradilistePregled> gradilista = DTOManager.GetGradnfo();
            IEnumerable<Vozilo> vozila = DTOManager.GetVoziloInfo();
            return vozila;
        }

        // GET: api/Vozilo/5
        public VoziloPregled Get(int id)
        {
            VoziloPregled vozilo = DTOManager.GetVoziloId(id);
            return vozilo;
        }

        // DELETE: api/Vozilo/5
        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteVozilo(id);
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
