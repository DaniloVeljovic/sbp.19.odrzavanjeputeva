﻿using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace odrzavanjePutevaF4.Controllers
{
    public class DeonicaController : ApiController
    {
        public IList<DeonicaPregled> Get()
        {
            //DTOManager dto = new DTOManager();
            //IEnumerable<GradilistePregled> gradilista = DTOManager.GetGradnfo();
            IList<DeonicaPregled> deonice = DTOManager.GetDeonInfo();

            return deonice;
        }
        public DeonicaPregledCeli Get(int id)
        {
            return DTOManager.UcitajInformacijeDeonicaPoId(id);
        }

        public int Post([FromBody]Deonica deonica)
        {
            if (DTOManager.DodajDeonicu(deonica))
                return 1;
            return -1;

        }

        public int Put(int id, [FromBody]DeonicaPregled deonica)
        {
            deonica.IdDeonice = id;
            return DTOManager.UpdateDeonicaPregled(deonica);
        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteDeonicu(id);
                return 1;

            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
