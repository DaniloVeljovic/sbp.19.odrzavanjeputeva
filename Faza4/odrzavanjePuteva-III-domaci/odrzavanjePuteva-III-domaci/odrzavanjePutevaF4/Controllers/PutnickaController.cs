﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class PutnickaController : ApiController
    {
        // GET: api/Putnicka
        public IEnumerable<PutnickaPregled> Get()
        {
            IEnumerable<PutnickaPregled> putnicka = DTOManager.LoadPutnicka();
            return putnicka;
        }

        // GET: api/Putnicka/5
        public PutnickaPregled Get(int id)
        {
            PutnickaPregled vozilo = DTOManager.LoadPutnickaId(id);
            return vozilo;
        }

        // POST: api/Putnicka
        public int Post([FromBody]PutnickaPregled vozilo)
        {
            if (DTOManager.CreatePutnicka(vozilo))
                return 1;
            return -1;
        }

        public int Put(int id, [FromBody]PutnickaPregled vozilo)
        {
            vozilo.IdVozila = id;
            if (DTOManager.UpdatePutnicka(vozilo))
                return 1;
            return -1;
        }

        public int Delete(int id)
        {
            try
            {
                DTOManager.DeletePutnicka(id);
                return 1;
            }
            catch (Exception e)
            {
                return -1;
            }
        }
    }
}
