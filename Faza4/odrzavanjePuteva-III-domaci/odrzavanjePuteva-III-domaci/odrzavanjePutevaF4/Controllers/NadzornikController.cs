﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class NadzornikController : ApiController
    {
        public IList<NadzornikPregled> Get()
        {
            IList<NadzornikPregled> np = DTOManager.GetNadzornikInfo();
            return np;
        }
        public NadzornikPregled Get(int id)
        {
            NadzornikPregled ip = DTOManager.GetNadzornikPregledId(id);
            return ip;
        }

        public int Post([FromBody]NadzornikPregled ip)
        {
            if (DTOManager.DodajNadzornik(ip.GodinaRodjenja, ip.Adresa, ip.Jmbg, ip.Ime, ip.Prezime, ip.OcevoIme))
            {
                return 1;
            }
            else
                return -1;

        }

        public int Put(int id, [FromBody]NadzornikPregled ip)
        {
            ip.IdRadnika = id;
            return DTOManager.UpdateNadzornikPregled(ip);

        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteNadzornikPregled(id);
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
