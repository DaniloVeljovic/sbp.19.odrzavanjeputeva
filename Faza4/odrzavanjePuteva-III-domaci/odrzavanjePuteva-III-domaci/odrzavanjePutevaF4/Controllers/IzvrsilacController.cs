﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;

namespace odrzavanjePutevaF4.Controllers
{
    public class IzvrsilacController : ApiController
    {
        
        public IList<IzvrsilacPregled> Get()
        {
            IList<IzvrsilacPregled> ip = DTOManager.GetIzvrsilacInfo();
            return ip;
        }
        public IzvrsilacPregled Get(int id)
        {
            IzvrsilacPregled ip = DTOManager.GetIzvrsilacPregledId(id);
            return ip;
        }

        public int Post([FromBody]IzvrsilacPregled ip)
        {
            if (DTOManager.DodajIzvrsioca(ip.GodinaRodjenja, ip.Adresa,ip.Jmbg,ip.Ime,ip.Prezime,ip.OcevoIme))
            {
                return 1;
            }
            else
                return -1;

        }

        public int Put(int id, [FromBody]IzvrsilacPregled ip)
        {
            ip.IdRadnika = id;
            return DTOManager.UpdateIzvrsilacPregled(ip);

        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteIzvrsilac(id);
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }


    }
}
