﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePutevaF4.Controllers
{
    public class SpoljniSaradnikController : ApiController
    {
        public IList<SpoljniSaradnikPregled> Get()
        {
            DTOManager dto = new DTOManager();
            //IEnumerable<GradilistePregled> gradilista = DTOManager.GetGradnfo();
            IList<SpoljniSaradnikPregled> ssp = DTOManager.GetSpoljniSInfo();

            return ssp;
        }
        public SpoljniSaradnikPregled Get(int id)
        {
            SpoljniSaradnikPregled ssp = DTOManager.GetSaradnikId(id);
            return ssp;
        }

        public int Post([FromBody]SpoljniSaradnikPregled ssp)
        {
            if (DTOManager.DodajSaradnika(ssp.BrojUgovoraODelu))
            {
                return 1;
            }
            else
                return -1;

        }

        public int Put(int id, [FromBody]SpoljniSaradnikPregled ssp)
        {
            ssp.IdSpoljnogSaradnika = id;
            return DTOManager.UpdateSpoljniSaradnikPregled(ssp);

        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteSpoljniSaradnikPregled(id);
                return 1;
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
    }
}
