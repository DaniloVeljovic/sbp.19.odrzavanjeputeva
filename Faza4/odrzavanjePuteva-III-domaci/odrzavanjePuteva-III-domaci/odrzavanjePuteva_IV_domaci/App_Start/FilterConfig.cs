﻿using System.Web;
using System.Web.Mvc;

namespace odrzavanjePuteva_IV_domaci
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
