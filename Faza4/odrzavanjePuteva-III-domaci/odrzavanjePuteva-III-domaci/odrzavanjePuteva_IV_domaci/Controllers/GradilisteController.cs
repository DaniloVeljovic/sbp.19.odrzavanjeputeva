﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using odrzavanjePuteva;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva_IV_domaci.Controllers
{
    public class GradilisteController : ApiController
    {
        public IEnumerable<GradilistePregled> Get()
        {
            IEnumerable<GradilistePregled> gradilista = DTOManager.GetGradnfo();
            return gradilista;
        }
        public GradilistePregled Get(int id)
        {
            GradilistePregled banka = DTOManager.GetGradilisteId(id);
            return banka;
        }

        public int Post([FromBody]Gradiliste gradiliste)
        {
            DTOManager d = new DTOManager();
           return  d.CreateGradilisteInt(gradiliste);
           
        }

        public int Put(int id, [FromBody]GradilistePregled gradilsite)
        {
            gradilsite.IdGradilista = id;
            return DTOManager.UpdateGradilistePregled(gradilsite);
             
        }


        public int Delete(int id)
        {
            try
            {
                DTOManager.DeleteGradiliste(id);
                return 1;
            }
            catch(Exception ex)
            {

                return -1;
            }
        }

        //// GET api/<controller>
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/<controller>/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //public void Delete(int id)
        //{
        //}
    }
}