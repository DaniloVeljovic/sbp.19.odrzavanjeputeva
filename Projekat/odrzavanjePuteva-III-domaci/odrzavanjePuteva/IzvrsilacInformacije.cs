﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class IzvrsilacInformacije : Form
    {
        public int IzvrsilacId { get; set; }
        public IzvrsilacInformacije()
        {
            InitializeComponent();
        }

        public IzvrsilacInformacije(int Id)
        {
            this.IzvrsilacId = Id;
            InitializeComponent();
        }


        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<IzvrsilacPregled> dInfos = DTOManager.GetIzvrsilacInfo();
            foreach (IzvrsilacPregled op in dInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdRadnika.ToString(), op.GodinaRodjenja, op.Adresa, op.Jmbg, op.Ime, op.Prezime });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void btnDeonica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite izvrsioca!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            IzvrsilacBasic ob = DTOManager.GetIzvrsilacBasic(Id);

            IzvrsilacEditBasic edbForm = new IzvrsilacEditBasic(ob);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //MessageBox.Show("snimanje podataka");
                DTOManager.UpdateIzvrsilacBasic(edbForm.iBasic);
                PopulateInfos();
            }
        }

        private void IzvrsilacInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.BackColor = Color.PaleGoldenrod;
        }


    }
}
