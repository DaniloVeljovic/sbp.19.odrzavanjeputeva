﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using odrzavanjePuteva.Mapiranja;

namespace odrzavanjePuteva
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
      
        }

        private void button31_Click(object sender, EventArgs e)
        {
            DeonicaInformacije deonica = new DeonicaInformacije(5);
            deonica.Show();
        }

        private void button32_Click(object sender, EventArgs e)
        {
            VozilaInformacije vozila = new VozilaInformacije();
            vozila.Show();
        }

        private void button33_Click(object sender, EventArgs e)
        {
            GradilisteInformacije gradiliste = new GradilisteInformacije();
            gradiliste.Show();
        }

        private void button34_Click(object sender, EventArgs e)
        { 
            IzvrsilacInformacije izv = new IzvrsilacInformacije();
           
            izv.Show();
        }

        private void button35_Click(object sender, EventArgs e)
        {
            IzvrsilacKreiranje izv = new IzvrsilacKreiranje();
           
            izv.Show();
        }

        private void button36_Click(object sender, EventArgs e)
        {
            SpoljniSaradnikInformacije S = new SpoljniSaradnikInformacije();
            S.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.BackgroundImage = Properties.Resources.putevi;

            this.button34.BackColor = Color.LightSkyBlue;     
            this.button31.BackColor = Color.LightSkyBlue;
            this.button32.BackColor = Color.LightSkyBlue;
            this.button33.BackColor = Color.LightSkyBlue;
            this.button35.BackColor = Color.LightSkyBlue;
            this.button36.BackColor = Color.LightSkyBlue;
          
        }
    }
}
