﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class IzvrsilacEditBasic : Form
    {
        public IzvrsilacBasic iBasic;
        public IzvrsilacEditBasic()
        {
            InitializeComponent();
        }

        public IzvrsilacEditBasic(IzvrsilacBasic ib)
        {
            this.iBasic = ib;
            InitializeComponent();
            PopulateData();
        }

        private void PopulateData()
        {
            txtOcevoIme.Text = iBasic.OcevoIme;
            txtMBS.Text = iBasic.MatBrSefa;
            txtIme.Text = iBasic.Ime;
            txtPrezime.Text = iBasic.Prezime;
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            
                iBasic.OcevoIme = txtOcevoIme.Text;
                iBasic.MatBrSefa = txtMBS.Text;
                iBasic.Ime = txtIme.Text;
                iBasic.Prezime = txtPrezime.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
        }

        private void IzvrsilacEditBasic_Load(object sender, EventArgs e)
        {
            //this.btnSnimi.BackColor = Color.PaleTurquoise;
          this.BackColor = Color.PaleGoldenrod;
        }
    }
}
