﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class IzvrsilacKreiranje : Form
    {
        public IzvrsilacKreiranje()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                Entiteti.Izvrsilac p = new Entiteti.Izvrsilac();
                MessageBox.Show(dtpGodina.Value.ToShortDateString());
                p.Adresa = txtAdresa.Text;
                //p.DatumPostavljanja =  //DateTime.Parse(txtGodina.Text);
                p.Ime = txtIme.Text;
                p.Prezime = txtPrezime.Text;
                p.Specijanost = txtSpecijalnost.Text;
                p.MatBrSefa = txtMatBrSefa.Text;
                p.OcevoIme = txtOcevo.Text;
                p.GodinaRodjenja = dtpGodina.Value.ToShortDateString();
                p.Jmbg = txtJMBG.Text;
                DTOManager.CreateIzvrsilac(p);
        }

        private void IzvrsilacKreiranje_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.PaleGoldenrod;
          //  this.button1.BackColor = Color.PaleTurquoise;
        }
    }
}
