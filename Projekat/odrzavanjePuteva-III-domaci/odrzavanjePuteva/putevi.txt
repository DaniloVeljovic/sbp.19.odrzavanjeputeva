using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva.Entiteti
{
   public class Upravlja
    {
        public virtual int IdUpravlja { get; protected set; }
        public virtual DateTime DatumOd { get; set; }
        public virtual DateTime DatumDo { get; set; }
       // public virtual Radnik Nadzornik { get; set; }
        public virtual Vozilo Vozilo { get; set; }

  
    }
}
---------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva.Entiteti
{
    public class Vozilo
    {
        public virtual int IdVozila { get; protected set; }
        public virtual string RegistarskaOznaka { get; set; }
        public virtual string TipGoriva { get; set; }
        public virtual string Boja { get; set; }
        public virtual int ZapreminaMotora { get; set; }
        public virtual string TipVozila { get; set; }
        public virtual string BrojMesta { get; set; }    //zasto za broj mesta VARCHAR??
        public virtual string Nosivost { get; set; }
        public virtual string BrojOsovina { get; set; }
        public virtual string TipRadneMasine { get; set; }
        public virtual string TipPogonaRadneMasine { get; set; }

       //  public virtual IList<Koriste> KoristePutnicko { get;  set; }
         public virtual IList<Upravlja> UpravljaIzvrsilac { get; set; }   //proveriti da li treba lista
      //   public virtual IList<Angazovane> AngazovaneMasine { get; set; }   //

        public Vozilo()
        {
            //  KoristePutnicko = new List<Koriste>();

             UpravljaIzvrsilac = new List<Upravlja>();

          //  AngazovaneMasine = new List<Angazovane>();
        }

    }

    public class Putnicka:Vozilo
    {

    }
    public class Teretna : Vozilo
    {

    }
    public class RadneMasine : Vozilo
    {

    }
}

--------------------------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using odrzavanjePuteva.Entiteti;
using FluentNHibernate.Mapping;

namespace odrzavanjePuteva.Mapiranja
{
    class UpravljaMapiranja:ClassMap<Upravlja>
    {

        public UpravljaMapiranja()
        {
            Table("UPRAVLJA");

            //mapiranje primarnog kljuca
            Id(x => x.IdUpravlja, "ID_UPRAVLJA").GeneratedBy.SequenceIdentity("ACA.UPRAVLJA_ID_SEQ");   //!!!!

            //mapiranje svojstava.
            Map(x => x.DatumOd, "DATUM_OD");
            Map(x => x.DatumDo, "DATUM_DO");

            //mapiranje veza
            References(x => x.Vozilo).Column("ID_VOZILA");
       //     References(x => x.Nadzornik).Column("ID_NADZORNIKA");

        }
    }
}

-------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using odrzavanjePuteva.Entiteti;
using FluentNHibernate.Mapping;

namespace odrzavanjePuteva.Mapiranja
{
    class VoziloMapiranja : ClassMap<Vozilo>
    {
        public VoziloMapiranja()
        {
            Table("VOZILO");
            DiscriminateSubClassesOnColumn("TIP_VOZILA");

            //mapiranje primarnog kljuca
            Id(x => x.IdVozila, "IDVOZILA").GeneratedBy.TriggerIdentity().UnsavedValue(-1); //

            //mapiranje svojstava
            //Map(x => x.Tip, "TIP");
            Map(x => x.RegistarskaOznaka, "REGISTARSKA_OZNAKA");
            Map(x => x.TipGoriva, "TIP_GORIVA");
            Map(x => x.Boja, "BOJA");
            Map(x => x.ZapreminaMotora, "ZAPREMINA_MOTORA");
            Map(x => x.BrojMesta, "BROJ_MESTA");
            Map(x => x.Nosivost, "NOSIVOST");
            Map(x => x.BrojOsovina, "BROJ_OSOVINA");
            Map(x => x.TipRadneMasine, "TIP_RADNE_MASINE");
            Map(x => x.TipPogonaRadneMasine, "TIP_POGONA_RADNE_MASINE");
            // Map(x => x.TipVozila, "TIP_VOZILA");

            //VEZE
           // HasMany(x => x.KoristePutnicko).KeyColumn("ID_VOZILA").LazyLoad().Cascade.All().Inverse();
            HasMany(x => x.UpravljaIzvrsilac).KeyColumn("ID_VOZILA").LazyLoad().Cascade.All().Inverse();
            //HasMany(x => x.AngazovaneMasine).KeyColumn("ID_VOZILA").LazyLoad().Cascade.All().Inverse();


        }


    }

    class Putnicka:SubclassMap<Vozilo>
    {
        public Putnicka()
        {
            DiscriminatorValue("putnicka");
        }

    }
    class Teretna : SubclassMap<Vozilo>
    {
        public Teretna()
        {
            DiscriminatorValue("teretna");
        }

    }
    class RadneMasine : SubclassMap<Vozilo>
    {
        public RadneMasine()
        {
            DiscriminatorValue("radne masine");   //proveriti
        }

    }
}

----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using odrzavanjePuteva.Mapiranja;

namespace odrzavanjePuteva
{
    class DataLayer
    {
        private static ISessionFactory _factory = null;
        private static object objLock = new object();


        //funkcija na zahtev otvara sesiju
        public static ISession GetSession()
        {
            //ukoliko session factory nije kreiran
            if (_factory == null)
            {
                lock (objLock)
                {
                    if (_factory == null)
                        _factory = CreateSessionFactory();
                }
            }

            return _factory.OpenSession();
        }

        //konfiguracija i kreiranje session factory
        private static ISessionFactory CreateSessionFactory()
        {
            try
            {
                var cfg = OracleClientConfiguration.Oracle10
                .ShowSql()
                .ConnectionString(c =>
                    c.Is("Data Source=gislab-oracle.elfak.ni.ac.rs:1521/SBP_PDB;User Id=S16300;Password=baze16300"));

                return Fluently.Configure()
                    .Database(cfg)
                    .Mappings(m => m.FluentMappings.AddFromAssemblyOf<VoziloMapiranja>())
                    .BuildSessionFactory();
            }
            catch (Exception ec)
            {
                System.Windows.Forms.MessageBox.Show(ec.Message);
                return null;
            }

        }
    }
}
-------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
namespace odrzavanjePuteva
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

               
                odrzavanjePuteva.Entiteti.Vozilo p = s.Load<odrzavanjePuteva.Entiteti.Vozilo>(4);

              //  MessageBox.Show(p.Boja);

                Upravlja o = s.Load<Upravlja>(5);

                MessageBox.Show(o.DatumOd.ToString());
               

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
    }
}
