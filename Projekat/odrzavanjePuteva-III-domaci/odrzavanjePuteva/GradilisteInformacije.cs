﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class GradilisteInformacije : Form
    {
        public GradilisteInformacije()
        {
            InitializeComponent();
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<GradilistePregled> gInfos = DTOManager.GetGradnfo();
            foreach (GradilistePregled op in gInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdGradilista.ToString(), op.TipGradilista.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void GradilisteInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.BackColor = Color.PaleGoldenrod;
        }
    }
}
