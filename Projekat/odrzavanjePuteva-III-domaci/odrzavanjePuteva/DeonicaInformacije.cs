﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class DeonicaInformacije : Form
    {
        public int DeonicaId { get; set; }
        public DeonicaInformacije()
        {
            InitializeComponent();
        }

        public DeonicaInformacije(int dId)
        {
            this.DeonicaId = dId;
            InitializeComponent();
        }


        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<DeonicaPregled> dInfos = DTOManager.GetDeonInfo(this.DeonicaId);
            foreach (DeonicaPregled op in dInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdDeonice.ToString(), op.OdKilometra.ToString(), op.DoKilometra.ToString()});

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void DeonicaInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
        //    this.BackColor = Color.PaleGoldenrod;
            
           
        }

        private void btnDeonica_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite deonicu");
                return;
            }

            int dId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            DeonicaBasic ob = DTOManager.GetDeonicaBasic(dId);

            DeonicaEditBasic edbForm = new DeonicaEditBasic(ob);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //MessageBox.Show("snimanje podataka");
                DTOManager.UpdateDeonicaBasic(edbForm.dBasic);
                PopulateInfos();
            }
        }
    }
}
