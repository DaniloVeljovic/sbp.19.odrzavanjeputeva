﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class VozilaInformacije : Form
    {
        public VozilaInformacije()
        {
            InitializeComponent();
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<PutnickaPregled> vInfos = DTOManager.GetVozInfo();
            foreach (PutnickaPregled op in vInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdVozila.ToString(), op.RegistarskaOznaka.ToString(), op.TipGoriva.ToString(), op.Boja.ToString(), op.ZapreminaMotora.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void VozilaInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.BackColor = Color.PaleGoldenrod;
        }
    }
}
