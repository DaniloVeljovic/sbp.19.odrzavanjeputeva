﻿namespace odrzavanjePuteva
{
    partial class DeonicaEditBasic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDoKilometra = new System.Windows.Forms.TextBox();
            this.txtOdGrada = new System.Windows.Forms.TextBox();
            this.txtDoGrada = new System.Windows.Forms.TextBox();
            this.btnSnimi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Do kilometra";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(180, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Od grada";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(328, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Do grada";
            // 
            // txtDoKilometra
            // 
            this.txtDoKilometra.Location = new System.Drawing.Point(12, 37);
            this.txtDoKilometra.Name = "txtDoKilometra";
            this.txtDoKilometra.Size = new System.Drawing.Size(100, 20);
            this.txtDoKilometra.TabIndex = 3;
            this.txtDoKilometra.TextChanged += new System.EventHandler(this.txtDoKilometra_TextChanged);
            // 
            // txtOdGrada
            // 
            this.txtOdGrada.Location = new System.Drawing.Point(158, 37);
            this.txtOdGrada.Name = "txtOdGrada";
            this.txtOdGrada.Size = new System.Drawing.Size(100, 20);
            this.txtOdGrada.TabIndex = 4;
            this.txtOdGrada.TextChanged += new System.EventHandler(this.txtOdGrada_TextChanged);
            // 
            // txtDoGrada
            // 
            this.txtDoGrada.Location = new System.Drawing.Point(302, 37);
            this.txtDoGrada.Name = "txtDoGrada";
            this.txtDoGrada.Size = new System.Drawing.Size(100, 20);
            this.txtDoGrada.TabIndex = 5;
            this.txtDoGrada.TextChanged += new System.EventHandler(this.txtDoGrada_TextChanged);
            // 
            // btnSnimi
            // 
            this.btnSnimi.Location = new System.Drawing.Point(169, 88);
            this.btnSnimi.Name = "btnSnimi";
            this.btnSnimi.Size = new System.Drawing.Size(75, 23);
            this.btnSnimi.TabIndex = 6;
            this.btnSnimi.Text = "Snimi";
            this.btnSnimi.UseVisualStyleBackColor = true;
            this.btnSnimi.Click += new System.EventHandler(this.btnSnimi_Click);
            // 
            // DeonicaEditBasic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 139);
            this.Controls.Add(this.btnSnimi);
            this.Controls.Add(this.txtDoGrada);
            this.Controls.Add(this.txtOdGrada);
            this.Controls.Add(this.txtDoKilometra);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "DeonicaEditBasic";
            this.Text = "DeonicaEditBasic";
            this.Load += new System.EventHandler(this.DeonicaEditBasic_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtDoKilometra;
        private System.Windows.Forms.TextBox txtOdGrada;
        private System.Windows.Forms.TextBox txtDoGrada;
        private System.Windows.Forms.Button btnSnimi;
    }
}