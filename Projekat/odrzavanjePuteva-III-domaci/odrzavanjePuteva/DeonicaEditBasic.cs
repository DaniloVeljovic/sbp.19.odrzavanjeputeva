﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class DeonicaEditBasic : Form
    {
        public DeonicaBasic dBasic;

        public DeonicaEditBasic()
        {
            InitializeComponent();
        }

        public DeonicaEditBasic(DeonicaBasic db)
        {
            this.dBasic = db;
            InitializeComponent();
            PopulateData();
        }

        private void PopulateData()
        {
            txtDoKilometra.Text = dBasic.DoKilometra.ToString();
            txtOdGrada.Text = dBasic.OdGrada.ToString();
            txtDoGrada.Text = dBasic.DoGrada;
        }

        private void txtDoKilometra_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtOdGrada_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDoGrada_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            try
            {
                dBasic.DoKilometra = Int32.Parse(txtDoKilometra.Text);
                dBasic.OdGrada = txtOdGrada.Text;
                dBasic.DoGrada = txtDoGrada.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Do kilometara mora biti ceo broj.");
            }
        }

        private void DeonicaEditBasic_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.PaleGoldenrod;
        }
    }
}
