﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using odrzavanjePuteva.Mapiranja;

namespace odrzavanjePuteva
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

               
                odrzavanjePuteva.Entiteti.Vozilo p = s.Load<odrzavanjePuteva.Entiteti.Vozilo>(4);

                MessageBox.Show(p.Boja);
               

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Putnicka p = new Entiteti.Putnicka();

                p.RegistarskaOznaka= "VEKSAUTO";
                p.TipGoriva = "dizel";
                p.Boja = "siva";
                //p.TipVozila = "putnicka";
                p.ZapreminaMotora = 2000;
                p.BrojMesta = 5;

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();


                odrzavanjePuteva.Entiteti.Deonica p = s.Load<odrzavanjePuteva.Entiteti.Deonica>(23);

                MessageBox.Show(p.OdGrada);


                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Upravlja o = s.Load<Upravlja>(5);

                MessageBox.Show(o.DatumOd.ToString());

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Angazovane o = s.Load<Angazovane>(2);

                MessageBox.Show(o.DatumOd.ToString());

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Koriste o = s.Load<Koriste>(5);

                MessageBox.Show(o.DatumOd.ToString());

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Radnik o = s.Load<Radnik>(24);

                MessageBox.Show(o.Ime.ToString());

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(5);

                MessageBox.Show(o.BrojUgovoraODelu.ToString());

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(5);

                foreach(Entiteti.Deonica d in o.Deonice)
                    MessageBox.Show(d.DoGrada);

                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Radnik p = new Entiteti.Radnik();

                p.Adresa = "Rasinska 11";
                p.DatumPostavljanja = DateTime.Today;
                p.Ime = "Marko";
                p.Jmbg = "1102997685418";
                p.Prezime = "Jovic";
                p.TipRadnika = "izvrsilac";
                p.Specijanost = "mostogradnja";
                p.MatBrSefa = "1001976546347";
                p.GodinaRodjenja = "1997";
                p.OcevoIme = "Stevan";

                odrzavanjePuteva.Entiteti.Deonica d = s.Load<odrzavanjePuteva.Entiteti.Deonica>(23);
                p.Deonice.Add(d);

                odrzavanjePuteva.Entiteti.Vozilo v = s.Load<odrzavanjePuteva.Entiteti.Vozilo>(4);
                Upravlja u = new Upravlja();
                u.DatumOd = DateTime.Today;
                u.DatumDo = DateTime.Today;

                u.RadnikIzvrsilac = p;
                u.Vozilo = v;
                p.UpravljaVozilima.Add(u);

                s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Radnik p = new Entiteti.Radnik();

                p.Adresa = "Martovska 8";
                p.DatumPostavljanja = DateTime.Parse("20-FEB-19");
                p.Ime = "Jordan";
                p.Jmbg = "1102997685418";
                p.Prezime = "Jovcev";
                p.TipRadnika = "nadzornik";
                p.MatBrSefa = "1001976546347";
                p.GodinaRodjenja = "1965";
                p.OcevoIme = "Mitka";
                s.Save(p);

                SpoljniSaradnik ss = new SpoljniSaradnik();
                ss.BrojUgovoraODelu = 11;
                ss.Nadzornik = p;
                s.Save(ss);

                odrzavanjePuteva.Entiteti.Vozilo v = s.Load<odrzavanjePuteva.Entiteti.Vozilo>(4);
                Koriste u = new Koriste();
                u.DatumOd = DateTime.Today;
                u.DatumDo = DateTime.Today;

                u.Nadzornik = p;
                u.Vozilo = v;
                p.KoristiVozila.Add(u);

                //s.Save(p);

                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Entiteti.Gradiliste p = new Entiteti.Gradiliste();
                p.TipGradilista = "Put";
                odrzavanjePuteva.Entiteti.Deonica d = s.Load<odrzavanjePuteva.Entiteti.Deonica>(41);
                p.Deonice.Add(d);
                odrzavanjePuteva.Entiteti.Deonica d1 = s.Load<odrzavanjePuteva.Entiteti.Deonica>(42);
                p.Deonice.Add(d1);
                odrzavanjePuteva.Entiteti.Deonica d2 = s.Load<odrzavanjePuteva.Entiteti.Deonica>(44);
                p.Deonice.Add(d2);
                odrzavanjePuteva.Entiteti.Deonica d3 = s.Load<odrzavanjePuteva.Entiteti.Deonica>(45);
                p.Deonice.Add(d3);

                s.Save(p);

                s.Flush();
                s.Close();
                
            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();

            Deonica d = new Deonica();
            d.OdGrada = "Nis";
            d.DoGrada = "Prokuplje";
            d.DoKilometra = 5;
            d.OdKilometra = 1;
            odrzavanjePuteva.Entiteti.Gradiliste p = s.Load<Gradiliste>(24);
            d.Gradiliste = p;
            odrzavanjePuteva.Entiteti.Radnik r = s.Load<Izvrsilac>(67);    //ne treba izvrsilac
            d.Izvrsilac = r;
            s.Save(d);

            Entiteti.Vozilo v = new Entiteti.Putnicka();
            v.RegistarskaOznaka = "NI0276";
            v.TipGoriva = "benzin";
            v.Boja = "siva";
            v.ZapreminaMotora = 2000;
            v.TipVozila = "putnicka";
            v.BrojMesta = 5;
            s.Save(v);

            Angazovane a = new Angazovane();
            a.DatumOd = DateTime.Parse("21-OCT-17");
            a.DatumDo = DateTime.Parse("21-NOV-17");
            a.Deonica = d;
            a.Vozilo = v;
            s.Save(a);

            v.AngazovaneMasine.Add(a);
            d.AngazovanaVozila.Add(a);
            s.Save(d);
            s.Save(v);

            s.Flush();
            s.Close();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();

            Angazovane a = new Angazovane();
            a.DatumOd = DateTime.Parse("21-JAN-17");
            a.DatumDo = DateTime.Parse("21-MAY-17");

            odrzavanjePuteva.Entiteti.Vozilo v = s.Load<odrzavanjePuteva.Entiteti.Vozilo>(21);
            a.Vozilo = v;
            odrzavanjePuteva.Entiteti.Deonica d  = s.Load<odrzavanjePuteva.Entiteti.Deonica>(41);

            a.Deonica = d;

            s.Save(a);

            s.Flush();
            s.Close();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Vozilo o = s1.Load<Vozilo>(46);
             
                o.TipVozila = "putnicko";
                
                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();
                
                Deonica o = s1.Load<Deonica>(49);

                o.DoGrada = "Prokuplje";
                Gradiliste g = s1.Load<Gradiliste>(3);
                o.Gradiliste = g;

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button17_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Upravlja o = s1.Load<Upravlja>(21);

                o.DatumDo = DateTime.Parse("03-MAY-2017");
                Vozilo v = s1.Load<Vozilo>(22);
                o.Vozilo = v;

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Angazovane o = s1.Load<Angazovane>(29);

                o.DatumDo = DateTime.Parse("03-MAY-2017");
                Vozilo v = s1.Load<Vozilo>(22);
                o.Vozilo = v;

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Koriste o = s1.Load<Koriste>(1);

                o.DatumDo = DateTime.Parse("03-MAY-2017");
                Vozilo v = s1.Load<Vozilo>(22);
                o.Vozilo = v;

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Radnik o = s1.Load<Radnik>(63);

                o.DatumPostavljanja = DateTime.Parse("03-MAY-2017");
                Radnik v = s1.Load<Radnik>(64);
                o.MatBrSefa = v.Jmbg;

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button21_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                SpoljniSaradnik o = s1.Load<SpoljniSaradnik>(6);

                o.BrojUgovoraODelu =6;
                

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s1 = DataLayer.GetSession();

                Gradiliste o = s1.Load<Gradiliste>(33);

                o.TipGradilista = "MOST";

                s1.Update(o);

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button23_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Vozilo o = s.Load<Vozilo>(23);

                
                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button24_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                //id
                Deonica o = s.Load<Deonica>(76);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button25_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Upravlja o = s.Load<Upravlja>(21);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button26_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Angazovane o = s.Load<Angazovane>(33);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Koriste o = s.Load<Koriste>(21);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        Radnik ucitajRadnika()
        {
            ISession s = DataLayer.GetSession();

            Radnik o = s.Load<Radnik>(65);
            MessageBox.Show(o.Ime);

            
            s.Flush();
            s.Close();
            return o;
        }

        private void button28_Click(object sender, EventArgs e)
        {
            try
            {

                Radnik r = ucitajRadnika();
                ISession s1 = DataLayer.GetSession();
                s1.Delete(r);
                //s.Delete("from Odeljenje");

                s1.Flush();
                s1.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button29_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(21);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(4);


                s.Delete(o);
                //s.Delete("from Odeljenje");

                s.Flush();
                s.Close();

            }
            catch (Exception ec)
            {
                MessageBox.Show(ec.Message);
            }
        }
    }
}
