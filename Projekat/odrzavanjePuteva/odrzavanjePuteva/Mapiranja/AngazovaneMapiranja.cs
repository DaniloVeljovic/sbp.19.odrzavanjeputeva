﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using odrzavanjePuteva.Entiteti;
using FluentNHibernate.Mapping;

namespace odrzavanjePuteva.Mapiranja
{
    class AngazovaneMapiranja : ClassMap<Angazovane>
    {
        public AngazovaneMapiranja()
        {
            Table("ANGAZOVANE");

            Id(x => x.IdAngazovane, "ID_ANGAZOVANE").GeneratedBy.TriggerIdentity();

            Map(x => x.DatumOd, "DATUM_OD");

            Map(x => x.DatumDo, "DATUM_DO");

            //HasManyToMany ide u vozilima i deonici
            //da li ovde treba REFERENCES ili nesto drugo posto je ovo tabela poveznica
            References(x => x.Vozilo).Column("ID_VOZILA"); //T

            References(x => x.Deonica).Column("ID_DEONICE"); //T
        }
    }
}
