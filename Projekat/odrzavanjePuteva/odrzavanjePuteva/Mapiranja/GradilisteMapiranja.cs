﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva.Mapiranja
{
    class GradilisteMapiranja : ClassMap<Gradiliste>
    {
        public GradilisteMapiranja()
        {

            //Mapiranje tabele
            Table("GRADILISTE");

            //mapiranje primarnog kljuca
            Id(x => x.IdGradilista, "IDGRADILISTA").GeneratedBy.TriggerIdentity();//.UnsavedValue(-1);

            //mapiranje svojstava
            Map(x => x.TipGradilista, "TIPGRADILISTA");

            //mapiranje 1:N
            HasMany(x => x.Deonice).KeyColumn("GRADILISTEID").LazyLoad().Cascade.All().Inverse();
        }

    }
}
