﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva.Mapiranja
{
    public class RadnikMapiranja : ClassMap<Radnik>
    {
        public RadnikMapiranja()
        {
            Table("RADNIK");

            Id(x => x.IdRadnika).Column("IDRADNIKA").GeneratedBy.TriggerIdentity();

            //mapiranje podklasa
            //podrazumevana vrednost je 0
            //svi radnici koji nisu sefovi ce imati vrednost 0 u koloni SEF_FLAG
            DiscriminateSubClassesOnColumn("TIPRADNIKA"); //PROVERA DA LI IMA 0
            Map(x => x.GodinaRodjenja).Column("GODINA_RODJENJA");
            Map(x => x.Adresa).Column("ADRESA");
            Map(x => x.Jmbg).Column("JMBG");
            Map(x => x.Prezime).Column("PREZIME");
            Map(x => x.Ime).Column("IME");
            Map(x => x.OcevoIme).Column("OCEVO_IME");
            Map(x => x.MatBrSefa).Column("MATBRSEFA");
            Map(x => x.Specijanost).Column("SPECIJALNOST");
            Map(x => x.DatumPostavljanja).Column("DATUM_POSTAVLJANJA");
           // Map(x => x.TipRadnika).Column("TIPRADNIKA");

            HasMany(x => x.UpravljaVozilima).KeyColumn("ID_IZVRSILAC").LazyLoad().Cascade.All().Inverse(); ;//T
            HasMany(x => x.KoristiVozila).KeyColumn("ID_NADZORNIKA").LazyLoad().Cascade.All().Inverse();
            HasMany(x => x.Deonice).KeyColumn("IDIZVRSIOCA").LazyLoad().Cascade.All().Inverse();  //provera      LazyLoad().Cascade.All();
            HasMany(x => x.SpoljniSaradnici).KeyColumn("ID_NADZORNIKA").LazyLoad().Cascade.All().Inverse();
        }
    }

    public class SefMapiranja : SubclassMap<Sef>
    {
        public SefMapiranja()
        {
            DiscriminatorValue(1);

            HasMany(x => x.SefujeRadnik).KeyColumn("MATBRSEFA").LazyLoad().Cascade.All().Inverse();

        }

    }

    public class NadzornikMapiranja : SubclassMap<Nadzornik>
    {
        public NadzornikMapiranja()
        {
            DiscriminatorValue("nadzornik");
        }
    }

    public class IzvrsilacMapiranja : SubclassMap<Izvrsilac>
    {
        public IzvrsilacMapiranja()
        {
            DiscriminatorValue("izvrsilac");
        //    HasMany(x => x.Deonice).KeyColumn("IDIZVRSIOCA").LazyLoad().Cascade.All().Inverse();  //proveriti


        }
    }
    
}

