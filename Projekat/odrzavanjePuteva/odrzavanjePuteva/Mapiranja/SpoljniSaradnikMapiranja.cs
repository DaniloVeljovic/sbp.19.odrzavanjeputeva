﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva.Mapiranja
{
    class SpoljniSaradnikMapiranja : ClassMap<SpoljniSaradnik>
    {
        public SpoljniSaradnikMapiranja()
        {
            Table("SPOLJNI_SARADNIK");

            Id(x => x.IdSpoljnogSaradnika, "ID_SPOLJNOG_SARADNIKA").GeneratedBy.TriggerIdentity();

            Map(x => x.BrojUgovoraODelu, "BROJ_UGOVORA_O_DELU");

            //na many strani ide references na one strani treba da ide HasMany
            References(x => x.Nadzornik).Column("ID_NADZORNIKA").LazyLoad();
        }
    }
}
