﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using odrzavanjePuteva.Entiteti;

namespace odrzavanjePuteva.Mapiranja
{
    class KoristeMapiranja : ClassMap<Koriste>
    {
        public KoristeMapiranja()
        {
            Table("KORISTE");

            Id(x => x.IdKoriste, "ID_KORISTE").GeneratedBy.TriggerIdentity();

            Map(x => x.DatumOd, "DATUM_OD");

            Map(x => x.DatumDo, "DATUM_DO");

            //HasManyToMany ide u vozilima i RADNIKU
            //da li ovde treba REFERENCES ili nesto drugo posto je ovo tabela poveznica
            References(x => x.Vozilo).Column("ID_VOZILA"); //T

            References(x => x.Nadzornik).Column("ID_NADZORNIKA"); //T
        }
    }
}
