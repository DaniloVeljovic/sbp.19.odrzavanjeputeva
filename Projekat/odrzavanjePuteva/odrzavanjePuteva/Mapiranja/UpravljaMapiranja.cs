﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using odrzavanjePuteva.Entiteti;
using FluentNHibernate.Mapping;

namespace odrzavanjePuteva.Mapiranja
{
    class UpravljaMapiranja:ClassMap<Upravlja>
    {

        public UpravljaMapiranja()
        {
            Table("UPRAVLJA");

            //mapiranje primarnog kljuca
            Id(x => x.IdUpravlja, "ID_UPRAVLJA").GeneratedBy.TriggerIdentity();   //GeneratedBy.SequenceIdentity("ACA.UPRAVLJA_ID_SEQ");   //!!!!

            //mapiranje svojstava.
            Map(x => x.DatumOd, "DATUM_OD");
            Map(x => x.DatumDo, "DATUM_DO");

            //mapiranje veza
            References(x => x.Vozilo).Column("ID_VOZILA"); //T
            References(x => x.RadnikIzvrsilac).Column("ID_IZVRSILAC"); //T

        }
    }
}
