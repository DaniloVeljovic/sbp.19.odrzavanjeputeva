﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class SpoljniSaradnikInformacije : Form
    {
        private List<SpoljniSaradnikPregled> listaSaradnika;
        public SpoljniSaradnikInformacije()
        {
            InitializeComponent();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            //  int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            int Id = listView1.SelectedItems[0].Index;
            /* SpoljniSaradnikIzmeni si = new SpoljniSaradnikIzmeni(Id);
             si.ShowDialog();
             if (si.DialogResult == DialogResult.OK)
                 PopulateInfos();*/
            ISession s = DataLayer.GetSession();

            SpoljniSaradnikPregled v = this.listaSaradnika.ElementAt(Id);
           
            s.Close();

            //   VoziloEdit edbForm = new VoziloEdit(v.IdVozila);
            SpoljniSaradnikIzmeni edbForm = new SpoljniSaradnikIzmeni(v);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //ISession s1 = DataLayer.GetSession();
                //s1.Save(edbForm.vBasic);
                //s.Close();
                PopulateInfos();
            }
        }

        private SpoljniSaradnik nadjiSaradnika(int id)
        {
            ISession s = DataLayer.GetSession();
            SpoljniSaradnik ss = s.Load<SpoljniSaradnik>(id);
            s.Close();
            return ss;
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<SpoljniSaradnikPregled> dInfos = DTOManager.GetSpoljniSInfo();
            foreach (SpoljniSaradnikPregled op in dInfos)
            {                                                      //op.IdSpoljnogSaradnika.ToString(),
                ListViewItem item = new ListViewItem(new string[] {  op.BrojUgovoraODelu.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void SpoljniSaradnikInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.listaSaradnika = DTOManager.GetSpoljniSInfo();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite izvrsioca!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
           
            SpoljniSaradnikPregled ob = DTOManager.GetSpoljni(Id);

            DTOManager.DeleteSpoljniSaradnikPregled(ob);
            PopulateInfos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpoljniSaradnikDodaj ss = new SpoljniSaradnikDodaj();
            ss.ShowDialog();
            if(ss.DialogResult == DialogResult.OK)
                PopulateInfos();
        }
    }
}
