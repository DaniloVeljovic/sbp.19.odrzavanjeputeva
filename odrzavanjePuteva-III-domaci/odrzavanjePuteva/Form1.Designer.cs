﻿namespace odrzavanjePuteva
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(26, 194);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(111, 23);
            this.button31.TabIndex = 31;
            this.button31.Text = "DTO DEONICE";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(570, 87);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(111, 23);
            this.button32.TabIndex = 32;
            this.button32.Text = "DTO VOZILA";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(26, 146);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(111, 23);
            this.button33.TabIndex = 33;
            this.button33.Text = "DTO GRADILISTE";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(570, 36);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(111, 23);
            this.button34.TabIndex = 34;
            this.button34.Text = "DTO IZVRSIOCI";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(26, 253);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(111, 48);
            this.button36.TabIndex = 36;
            this.button36.Text = "DTO SPOLJNI SARADNIK";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(715, 374);
            this.Controls.Add(this.button36);
            this.Controls.Add(this.button34);
            this.Controls.Add(this.button33);
            this.Controls.Add(this.button32);
            this.Controls.Add(this.button31);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button36;
    }
}

