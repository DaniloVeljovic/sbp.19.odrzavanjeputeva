﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class GradilisteInformacije : Form
    {
        public int GradilisteId { get; set; }

        private List<GradilistePregled> listaGradilista;
        public GradilisteInformacije()
        {
            InitializeComponent();
        }
        public GradilisteInformacije(int id)
        {
            this.GradilisteId = id;
            InitializeComponent();
        }
        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<GradilistePregled> gInfos = DTOManager.GetGradnfo();
            foreach (GradilistePregled op in gInfos)
            {                                                       //op.IdGradilista.ToString(),
                ListViewItem item = new ListViewItem(new string[] {  op.TipGradilista.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void GradilisteInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.listaGradilista = DTOManager.GetGradnfo();  
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            GradilisteKreiranje gk = new GradilisteKreiranje();
            if (gk.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PopulateInfos();
            }
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite gradiliste");
                return;
            }

            //  int gradilisteId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            int gradilisteId = listView1.SelectedItems[0].Index;
            //  GradilisteBasic gp = DTOManager.GetGradilisteBasic(gradilisteId);
            GradilistePregled gp = listaGradilista.ElementAt(gradilisteId);
            GradilisteEdit edbForm = new GradilisteEdit(gp);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //MessageBox.Show("snimanje podataka");
                DTOManager.UpdateGradilisteBasic(edbForm.dBasic);
                PopulateInfos();
            }
            /*GradilisteKreiranje gk = new GradilisteKreiranje();
            if (gk.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PopulateInfos();
            } */
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //brisanje
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite izvrsioca!");
                return;
            }

              int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            
            DTOManager.DeleteGradiliste(Id);
            PopulateInfos();
        }
    }
}
