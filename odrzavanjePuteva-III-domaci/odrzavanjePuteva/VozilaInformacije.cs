﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class VozilaInformacije : Form
    {
        private List<PutnickaPregled> listaVozila;
        public VozilaInformacije()
        {
            InitializeComponent();
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<PutnickaPregled> vInfos = DTOManager.GetVozInfo();
            foreach (PutnickaPregled op in vInfos)
            {                                                       //op.IdVozila.ToString(), ovo obrisano
                ListViewItem item = new ListViewItem(new string[] {  op.RegistarskaOznaka.ToString(), op.TipGoriva.ToString(), op.Boja.ToString(), op.ZapreminaMotora.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void VozilaInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            this.listaVozila =DTOManager.GetVozInfo();  //lista putnickapregled
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            VoziloDodaj edbForm = new VoziloDodaj();
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PopulateInfos();
            }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite vozilo!");
                return;
            }

                //  int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            int Id = listView1.SelectedItems[0].Index;
            //IzvrsilacBasic ob = DTOManager.GetIzvrsilacBasic(Id);
            ISession s = DataLayer.GetSession();

            PutnickaPregled v = listaVozila.ElementAt(Id);
          //  MessageBox.Show("VoziloInfo-"+v.IdVozila + " " + v.RegistarskaOznaka);
                  s.Close();

            //   VoziloEdit edbForm = new VoziloEdit(v.IdVozila);
            VoziloEdit edbForm = new VoziloEdit(v);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                  {
                      //ISession s1 = DataLayer.GetSession();
                      //s1.Save(edbForm.vBasic);
                      //s.Close();
                      PopulateInfos();
                  }
        }

        //probaj
        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite vozilo!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            //IzvrsilacBasic ob = DTOManager.GetIzvrsilacBasic(Id);
            ISession s = DataLayer.GetSession();
            Putnicka vv = s.Get<Putnicka>(Id);
            s.Delete(vv);
            s.Flush();
            s.Close();
            PopulateInfos();
        }

        private void ListView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
