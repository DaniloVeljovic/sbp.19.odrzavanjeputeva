﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class SpoljniSaradnikIzmeni : Form
    {
        private SpoljniSaradnikPregled ob1;

        public SpoljniSaradnikIzmeni()
        {
            InitializeComponent();
        }

        public SpoljniSaradnikIzmeni(SpoljniSaradnikPregled sp)
        {
            InitializeComponent();
            //   SpoljniSaradnik s = GetSpoljni(ob);
            //  this.ob1 = s;
            textBox1.Text = sp.BrojUgovoraODelu.ToString();
            comboBox1.Items.Clear();
            Nadzornik nadzornik = getNadzornik(sp.Nadzornik.IdRadnika);
            comboBox1.SelectedText = nadzornik.Jmbg;
            List<Nadzornik> nad = DTOManager.GetNadzornici();
            foreach (Nadzornik op in nad)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
                listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }
            comboBox1.Refresh();
            listBox1.Refresh();
          //  this.PopulateData(izv);
        }



        private void SpoljniSaradnikIzmeni_Load(object sender, EventArgs e)
        {
           /* textBox1.Text = ob1.BrojUgovoraODelu.ToString();
            comboBox1.Items.Clear();
            Nadzornik nadzornik = getNadzornik(ob1.Nadzornik.IdRadnika);
            comboBox1.SelectedText = nadzornik.Jmbg;
            List<Nadzornik> nad = DTOManager.GetNadzornici();

            //{op.Ime} {op.Prezime} 

            foreach (Nadzornik op in nad)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
                listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }
            comboBox1.Refresh();
            listBox1.Refresh();*/


        }

        private SpoljniSaradnik GetSpoljni(int id)
        {
            ISession s = DataLayer.GetSession();
            SpoljniSaradnik ret = s.Get<SpoljniSaradnik>(id);
            s.Close();
            return ret;
        }

        private Nadzornik getNadzornik(int id)
        {
            ISession s = DataLayer.GetSession();
            Nadzornik ret = s.Get<Nadzornik>(id);
            s.Close();
            return ret;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                SpoljniSaradnik ss = s.Get<SpoljniSaradnik>(ob1.IdSpoljnogSaradnika);
                ss.BrojUgovoraODelu = Convert.ToInt32(textBox1.Text);
                ss.Nadzornik = DTOManager.GetNadzornik(comboBox1.SelectedItem.ToString());
                s.Update(ss);
                s.Flush();
                s.Close();
                this.Close();
                this.DialogResult = DialogResult.OK;
            }
            catch(Exception ev)
            {

            }
            
        }
    }
}
