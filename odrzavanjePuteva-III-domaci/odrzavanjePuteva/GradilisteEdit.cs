﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class GradilisteEdit : Form
    {
        public GradilistePregled dBasic;    //grdilisteBasic
        public GradilisteEdit()
        {
            InitializeComponent();
        }
        public GradilisteEdit(GradilistePregled gp)  //GradilisteBasic
        {
            this.dBasic = gp;
            InitializeComponent();
            PopulateData();
        }
        private void PopulateData()
        {
            txtTip.Text = dBasic.TipGradilista.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                dBasic.TipGradilista = txtTip.Text;

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Greska.");
            }
        }

        private void GradilisteEdit_Load(object sender, EventArgs e)
        {

        }
    }
}
