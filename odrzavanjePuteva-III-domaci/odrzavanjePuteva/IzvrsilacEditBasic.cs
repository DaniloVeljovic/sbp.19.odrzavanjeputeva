﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using NHibernate;


namespace odrzavanjePuteva
{
    public partial class IzvrsilacEditBasic : Form
    {
        public IzvrsilacPregled iBasic;   //basic
        public IzvrsilacEditBasic()
        {
            InitializeComponent();
        }

        public IzvrsilacEditBasic(IzvrsilacPregled ib)
        {
            this.iBasic = ib;
            InitializeComponent();
            PopulateData();
        }

        private void PopulateData()
        {
            

            txtOcevoIme.Text = iBasic.OcevoIme;
            txtMBS.Text = iBasic.MatBrSefa;
            txtIme.Text = iBasic.Ime;
            txtPrezime.Text = iBasic.Prezime;

            ISession s = DataLayer.GetSession();

            Izvrsilac izv = s.Get<Izvrsilac>(iBasic.IdRadnika);

            s.Close();

            lbxDeonica.Items.Clear();
            List<DeonicaPregled> dInfos = DTOManager.GetDeonicaInfo(izv);
            int i = 1;
            foreach (DeonicaPregled op in dInfos)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});

                lbxDeonica.Items.Add($"{i}) {op.OdKilometra} {op.DoKilometra} {op.OdGrada} {op.DoGrada}");
                i++;
            }
            lbxDeonica.Refresh();

            this.osveziListBox();

            comboBox1.Items.Clear();
            List<VoziloPregled> v1Infos = DTOManager.GetVozilaInfo();
            int i2 = 1;
            foreach (VoziloPregled op in v1Infos)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});

                comboBox1.Items.Add($"{op.RegistarskaOznaka}");
                i2++;
            }
            comboBox1.Refresh();
        }

        private void osveziListBox()
        {
            ISession s = DataLayer.GetSession();

            Izvrsilac izv = s.Get<Izvrsilac>(iBasic.IdRadnika);

            


            listBox1.Items.Clear();
            List<Vozilo> vInfos = DTOManager.GetVozilaInfo(iBasic.IdRadnika);

            int i1 = 1;
            foreach(Upravlja u in izv.UpravljaVozilima)
            {
                Vozilo op = new Vozilo();
                foreach (Vozilo nadd in vInfos)
                {
                    if (u.Vozilo.IdVozila == nadd.IdVozila)
                        op = nadd;
                }

                string s1 = $"Od datuma: {u.DatumOd} do datuma: {u.DatumDo} - {op.RegistarskaOznaka} {op.Boja} {op.TipGoriva} {op.ZapreminaMotora}";
                listBox1.Items.Add(s1);
                i1++;
            }

            /*foreach (VoziloPregled op in vInfos)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});

                listBox1.Items.Add($"{i1}) {op.RegistarskaOznaka} {op.Boja} {op.TipGoriva} {op.ZapreminaMotora}");
                i1++;
            }*/
            listBox1.Refresh();
            s.Close();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            
                iBasic.OcevoIme = txtOcevoIme.Text;
                iBasic.MatBrSefa = txtMBS.Text;
                iBasic.Ime = txtIme.Text;
                iBasic.Prezime = txtPrezime.Text;
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

        private void IzvrsilacEditBasic_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();

            Izvrsilac izv = s.Load<Izvrsilac>(iBasic.IdRadnika);

            Upravlja up = new Upravlja();
            up.RadnikIzvrsilac = izv;

            Vozilo voz = DTOManager.GetVoziloRegNo(comboBox1.SelectedItem.ToString());


            up.Vozilo = voz;
            up.DatumOd = DateTime.Parse(textBox1.Text);
            up.DatumOd = DateTime.Parse(textBox2.Text);
            s.Save(up);
            s.Flush();
            s.Close();

            this.osveziListBox();
        }
    }
}
