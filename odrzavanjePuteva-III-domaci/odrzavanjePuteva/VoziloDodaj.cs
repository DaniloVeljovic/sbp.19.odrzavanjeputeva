﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class VoziloDodaj : Form
    {
        public Putnicka vBasic;
        public VoziloDodaj()
        {
            InitializeComponent();
            vBasic = new Putnicka();
        }

        public VoziloDodaj(int id)
        {
            InitializeComponent();

            vBasic = new Putnicka();
        }

        private void btnSnimi_Click(object sender, EventArgs e)
        {
            vBasic.Boja = txtBoja.Text;
            vBasic.BrojMesta = Convert.ToInt32(txtBrojMesta.Text);
            vBasic.RegistarskaOznaka = txtRegNo.Text;
            vBasic.TipGoriva = txtTipG.Text;
            vBasic.ZapreminaMotora = Convert.ToInt32(txtZapremina.Text);

            ISession s = DataLayer.GetSession();
            s.Save(vBasic);
            s.Flush();
            s.Close();
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }
    }
}
