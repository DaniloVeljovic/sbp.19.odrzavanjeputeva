﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class IzvrsilacKreiranje : Form
    {
        public IzvrsilacKreiranje()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
                Entiteti.Izvrsilac p = new Entiteti.Izvrsilac();
                
                p.Adresa = txtAdresa.Text;
                
                p.Ime = txtIme.Text;
                p.Prezime = txtPrezime.Text;
                p.Specijanost = txtSpecijalnost.Text;
                MessageBox.Show(cbxMatBrSefa.SelectedItem.ToString());
                p.MatBrSefa = cbxMatBrSefa.SelectedItem.ToString();
                p.OcevoIme = txtOcevo.Text;
                p.GodinaRodjenja = dtpGodina.Value.ToShortDateString();
                p.Jmbg = txtJMBG.Text;
                DTOManager.CreateIzvrsilac(p);

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();
        }

        private void IzvrsilacKreiranje_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
        }

        private void PopulateInfos()
        {
            cbxMatBrSefa.Items.Clear();
            List<IzvrsilacPregled> dInfos = DTOManager.GetIzvrsilacInfo();
            foreach (IzvrsilacPregled op in dInfos)
            {
                //ListViewItem item = new ListViewItem(new string[] {op.Jmbg});

                cbxMatBrSefa.Items.Add(op.Jmbg);
            }
            cbxMatBrSefa.Refresh();
        }

        private void cbxMatBrSefa_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
