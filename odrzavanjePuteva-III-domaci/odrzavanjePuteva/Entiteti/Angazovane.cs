﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva.Entiteti
{
        public class Angazovane
        {
            public virtual DateTime DatumOd { get; set; }
            public virtual DateTime DatumDo { get; set; }
            public virtual Vozilo Vozilo { get; set; }
            public virtual Deonica Deonica { get; set; }
            public virtual int IdAngazovane { get; set; }
        }
    
}
