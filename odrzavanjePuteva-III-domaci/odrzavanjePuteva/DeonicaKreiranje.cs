﻿using NHibernate;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class DeonicaKreiranje : Form
    {
        List<Gradiliste> grad;
        public DeonicaKreiranje()
        {
            InitializeComponent();
            grad = new List<Gradiliste>();
            this.populateDeonica();
        }

        private void populateDeonica()
        {
            ISession s = DataLayer.GetSession();

            //doradi gradilista da prikazuje i ostale selecte
            IEnumerable<Gradiliste> deonice = from o in s.Query<Gradiliste>()
                                           select o;

            foreach (Gradiliste o in deonice)
            {
                grad.Add(o);
            }

            s.Close();

            List<Izvrsilac> iz = DTOManager.GetIzvrsilac();
            comboBox1.Items.Clear();
            foreach (Izvrsilac op in iz)
            {
                comboBox1.Items.Add($"{op.Jmbg}");
                listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }

            comboBox1.Refresh();
            listBox1.Refresh();

            comboBox2.Items.Clear();
            foreach (Gradiliste op in grad)
            {
                comboBox2.Items.Add($"{op.TipGradilista}");
                //listBox1.Items.Add($"{op.Ime} {op.Prezime} {op.Jmbg}");
            }

            comboBox2.Refresh();
        }

        private void DeonicaKreiranje_Load(object sender, EventArgs e)
        {
            
            
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            ISession s = DataLayer.GetSession();
            Deonica dBasic = new Deonica();
            dBasic.Izvrsilac = DTOManager.getIzvrsilacJMBG(comboBox1.SelectedItem.ToString());
            dBasic.OdGrada = txtOdGrada.Text;
            dBasic.DoGrada = txtDoGrada.Text;
            dBasic.OdKilometra = Convert.ToInt32(txtOdKilometra.Text);
            dBasic.DoKilometra = Convert.ToInt32(txtDoKilometra.Text);
            dBasic.DatumOd = dtpDatumOd.Value;
            dBasic.DatumDo = dtpDatumDo.Value;
            Gradiliste g = new Gradiliste();
            foreach(Gradiliste gg in grad)
            {
                if (gg.TipGradilista == comboBox2.SelectedItem.ToString())
                    g = gg;
            }

            Gradiliste ggg = s.Get<Gradiliste>(g.IdGradilista);
            dBasic.Gradiliste = ggg;
            ggg.Deonice.Add(dBasic);
            s.Save(dBasic);
            s.Flush();


            ggg.Deonice.Add(dBasic);
            s.Update(ggg);
            s.Flush();
            s.Close();
            this.DialogResult = DialogResult.OK;
            this.Close();
            
        }
    }
}
