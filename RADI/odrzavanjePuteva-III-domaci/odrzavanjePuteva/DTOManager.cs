﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Linq;
using odrzavanjePuteva.Entiteti;
using NHibernate;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public class DTOManager
    {
        public static List<DeonicaPregled> GetDeonInfo()
        {
            List<DeonicaPregled> deoInfos = new List<DeonicaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Deonica> deonice = from o in s.Query<Deonica>()
                                               select o;

                foreach (Deonica o in deonice)
                {
                    deoInfos.Add(new DeonicaPregled(o.IdDeonice, o.OdKilometra, o.DoKilometra, o.OdGrada, o.DoGrada));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return deoInfos;
        }

        public static void DeleteGradiliste(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static void CreateGradiliste(Gradiliste gradiliste)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(gradiliste);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {

            }
        }

        public static GradilisteBasic UpdateGradilisteBasic(GradilisteBasic gb)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(gb.IdGradilista);
                o.TipGradilista = gb.TipGradilista;


                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return gb;
        }

        internal static GradilisteBasic GetGradilisteBasic(int gradilisteId)
        {
            GradilisteBasic db = new GradilisteBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Gradiliste o = s.Load<Gradiliste>(gradilisteId);
                db = new GradilisteBasic(o.IdGradilista, o.TipGradilista);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        internal static List<Izvrsilac> GetIzvrsilac()
        {
            List<Izvrsilac> nad = new List<Izvrsilac>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> izv = from o in s.Query<Izvrsilac>()
                                             select o;

                foreach (Izvrsilac o in izv)
                {
                    nad.Add(o);
                }

                s.Close();
            }
            catch (Exception e)
            {

            }

            return nad;
        }

        internal static Izvrsilac getIzvrsilacJMBG(string v)
        {
            Izvrsilac vozInfos = new Izvrsilac();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> vozila = from o in s.Query<Izvrsilac>()
                                                where o.Jmbg == v
                                                select o;

                foreach (Izvrsilac vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        internal static Izvrsilac GetIzvrsilac(int idRadnika)
        {
            ISession s = DataLayer.GetSession();
            Izvrsilac ret = s.Get<Izvrsilac>(idRadnika);
            s.Close();
            return ret;
        }

        public static List<DeonicaPregled> GetDeonicaInfo(Izvrsilac id)
        {
            List<DeonicaPregled> deoInfos = new List<DeonicaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Deonica> deonice = from o in s.Query<Deonica>()
                                               where o.Izvrsilac == id
                                               select o;

                foreach (Deonica o in deonice)
                {
                    deoInfos.Add(new DeonicaPregled(o.IdDeonice, o.DoKilometra, o.DoKilometra, o.OdGrada, o.DoGrada));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return deoInfos;
        }

        internal static List<Nadzornik> GetNadzornici(int idVozila)
        {
            List<Nadzornik> vozInfos = new List<Nadzornik>();
            try
            {
                ISession s = DataLayer.GetSession();

                Vozilo id = s.Load<Vozilo>(idVozila);

                foreach (Koriste up in id.RadniciKoriste)
                {
                    IEnumerable<Nadzornik> vozila = from o in s.Query<Nadzornik>()
                                                 where up.Nadzornik == o
                                                 select o;

                    foreach (Nadzornik o in vozila)
                    {

                        vozInfos.Add(o);
                    }
                }


                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Nadzornik GetNadzornik(string v)
        {
            Nadzornik vozInfos = new Nadzornik();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Nadzornik> vozila = from o in s.Query<Nadzornik>()
                                             where o.Jmbg == v
                                             select o;

                foreach (Nadzornik vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Deonica GetDeonica(int dId)
        {
            ISession s = DataLayer.GetSession();
            Deonica ret = s.Get<Deonica>(dId);
            s.Close();
            return ret;
        }

        public static List<Nadzornik> GetNadzornici()
        {
            List<Nadzornik> nad = new List<Nadzornik>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Nadzornik> izv = from o in s.Query<Nadzornik>()
                                             select o;

                foreach (Nadzornik o in izv)
                {
                    nad.Add(o);
                }

                s.Close();
            }
            catch (Exception e)
            {

            }

            return nad;
        }

        public static DeonicaBasic GetDeonicaBasic(int dId)
        {
            DeonicaBasic db = new DeonicaBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(dId);
                db = new DeonicaBasic(o.IdDeonice, o.DoKilometra, o.OdGrada, o.DoGrada);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        public static DeonicaBasic UpdateDeonicaBasic(DeonicaBasic db)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Deonica o = s.Load<Deonica>(db.deonicaId);
                o.DoKilometra = db.DoKilometra;
                o.OdGrada = db.OdGrada;
                o.DoGrada = db.DoGrada;

                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return db;
        }

        public static List<PutnickaPregled> GetVozInfo()
        {
            List<PutnickaPregled> vozInfos = new List<PutnickaPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Putnicka> vozila = from o in s.Query<Putnicka>()
                                               select o;

                foreach (Vozilo o in vozila)
                {
                    vozInfos.Add(new PutnickaPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora, o.BrojMesta));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static Vozilo GetVoziloRegNo(string v)
        {
            Vozilo vozInfos = new Vozilo();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                             where o.RegistarskaOznaka == v
                                             select o;

                foreach(Vozilo vo in vozila)
                {
                    vozInfos = vo;
                }
                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

       

        public static List<GradilistePregled> GetGradnfo()
        {
            List<GradilistePregled> gradInfos = new List<GradilistePregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Gradiliste> gradiliste = from o in s.Query<Gradiliste>()
                                                     select o;

                foreach (Gradiliste o in gradiliste)
                {
                    gradInfos.Add(new GradilistePregled(o.IdGradilista, o.TipGradilista));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return gradInfos;
        }

        public static List<IzvrsilacPregled> GetIzvrsilacInfo()
        {
            List<IzvrsilacPregled> izvInfos = new List<IzvrsilacPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Izvrsilac> izv = from o in s.Query<Izvrsilac>()
                                             select o;

                foreach (Izvrsilac o in izv)
                {
                    izvInfos.Add(new IzvrsilacPregled(o.IdRadnika, o.GodinaRodjenja, o.Adresa, o.Jmbg, o.Ime, o.Prezime));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return izvInfos;
        }

        public static IzvrsilacBasic GetIzvrsilacBasic(int dId)
        {
            IzvrsilacBasic db = new IzvrsilacBasic();
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(dId);
                db = new IzvrsilacBasic(o.IdRadnika, o.OcevoIme, o.MatBrSefa, o.Ime, o.Prezime);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        public static IzvrsilacBasic UpdateIzvrsilacBasic(IzvrsilacBasic db)
        {

            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(db.IdRadnika);
                o.Ime = db.Ime;
                o.MatBrSefa = db.MatBrSefa;
                o.OcevoIme = db.OcevoIme;
                o.Prezime = db.Prezime;


                s.Update(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }

            return db;
        }

        public static List<SpoljniSaradnikPregled> GetSpoljniSInfo()
        {
            List<SpoljniSaradnikPregled> spoljInfos = new List<SpoljniSaradnikPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<SpoljniSaradnik> saradnici = from o in s.Query<SpoljniSaradnik>()
                                                         select o;

                foreach (SpoljniSaradnik o in saradnici)
                {
                    spoljInfos.Add(new SpoljniSaradnikPregled(o.IdSpoljnogSaradnika, o.BrojUgovoraODelu));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return spoljInfos;
        }

        public static void DeleteSpoljniSaradnikPregled(SpoljniSaradnikPregled ss)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Load<SpoljniSaradnik>(ss.IdSpoljnogSaradnika);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static SpoljniSaradnikPregled GetSpoljni(int dId)
        {
            SpoljniSaradnikPregled db = new SpoljniSaradnikPregled();
            try
            {
                ISession s = DataLayer.GetSession();

                SpoljniSaradnik o = s.Get<SpoljniSaradnik>(dId);
                db = new SpoljniSaradnikPregled(o.IdSpoljnogSaradnika, o.BrojUgovoraODelu);

                s.Close();
            }
            catch (Exception e)
            {

            }
            return db;
        }

        public static void CreateIzvrsilac(Izvrsilac izvrsilac)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                s.Save(izvrsilac);
                s.Flush();
                s.Close();
            }
            catch (Exception ec)
            {
                
            }
        }

        public static void DeleteIzvrsilacPregled(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac o = s.Load<Izvrsilac>(id);

                s.Delete(o);
                s.Flush();

                s.Close();

            }
            catch (Exception ec)
            {
                //handle exceptions
            }
        }

        public static List<Vozilo> GetVozilaInfo(int id1)
        {
            List<Vozilo> vozInfos = new List<Vozilo>();
            try
            {
                ISession s = DataLayer.GetSession();

                Izvrsilac id = s.Load<Izvrsilac>(id1);

                foreach (Upravlja up in id.UpravljaVozilima)
                {
                    IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                                 where up.Vozilo == o
                                                 select o;

                    foreach (Vozilo o in vozila)
                    {
                        vozInfos.Add(o);
                        //vozInfos.Add(new VoziloPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora));
                    }
                }
                

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }

        public static List<VoziloPregled> GetVozilaInfo()
        {
            List<VoziloPregled> vozInfos = new List<VoziloPregled>();
            try
            {
                ISession s = DataLayer.GetSession();

                IEnumerable<Vozilo> vozila = from o in s.Query<Vozilo>()
                                               select o;

                foreach (Vozilo o in vozila)
                {
                    vozInfos.Add(new VoziloPregled(o.IdVozila, o.RegistarskaOznaka, o.TipGoriva, o.Boja, o.ZapreminaMotora));
                }

                s.Close();
            }
            catch (Exception e)
            {

            }
            return vozInfos;
        }
    }
}
