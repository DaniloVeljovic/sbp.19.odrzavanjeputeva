﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class VozilaInformacije : Form
    {
        public VozilaInformacije()
        {
            InitializeComponent();
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<PutnickaPregled> vInfos = DTOManager.GetVozInfo();
            foreach (PutnickaPregled op in vInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdVozila.ToString(), op.RegistarskaOznaka.ToString(), op.TipGoriva.ToString(), op.Boja.ToString(), op.ZapreminaMotora.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void VozilaInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            listView1.Columns[0].Width = 0;
        }

        private void btnDodaj_Click(object sender, EventArgs e)
        {
            VoziloDodaj edbForm = new VoziloDodaj();
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PopulateInfos();
            }
        }

        private void btnIzmeni_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite vozilo!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            //IzvrsilacBasic ob = DTOManager.GetIzvrsilacBasic(Id);
            ISession s = DataLayer.GetSession();

            Putnicka v = s.Get<Putnicka>(Id);
            s.Close();

            VoziloEdit edbForm = new VoziloEdit(v.IdVozila);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                //ISession s1 = DataLayer.GetSession();
                //s1.Save(edbForm.vBasic);
                //s.Close();
                PopulateInfos();
            }
        }

        //probaj
        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite vozilo!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            //IzvrsilacBasic ob = DTOManager.GetIzvrsilacBasic(Id);
            ISession s = DataLayer.GetSession();
            Putnicka vv = s.Get<Putnicka>(Id);
            s.Delete(vv);
            s.Flush();
            s.Close();
            PopulateInfos();
        }

        private void VozilaInformacije_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 10),
                            this.DisplayRectangle);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
