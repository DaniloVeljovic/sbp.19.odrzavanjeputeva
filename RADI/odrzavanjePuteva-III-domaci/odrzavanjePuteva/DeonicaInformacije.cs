﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class DeonicaInformacije : Form
    {
        public int DeonicaId { get; set; }
        public DeonicaInformacije()
        {
            InitializeComponent();
        }

        public DeonicaInformacije(int dId)
        {
            this.DeonicaId = dId;
            InitializeComponent();
        }


        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<DeonicaPregled> dInfos = DTOManager.GetDeonInfo();
            foreach (DeonicaPregled op in dInfos)
            {
                ListViewItem item = new ListViewItem(new string[] { op.IdDeonice.ToString(), op.OdKilometra.ToString(), op.DoKilometra.ToString()});

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void DeonicaInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
            listView1.Columns[0].Width = 0;
        }

        private void btnDeonica_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnDodaj_Click(object sender, EventArgs e)
        {
            DeonicaKreiranje dk = new DeonicaKreiranje();
            if (dk.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                PopulateInfos();
            }
        }

        private void BtnEdit_Click(object sender, EventArgs e)
        {

            /*if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite deonicu");
                return;
            }

            int dId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            DeonicaBasic ob = DTOManager.GetDeonicaBasic(dId);

            DeonicaEdit edbForm = new DeonicaEdit(ob);
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                DTOManager.UpdateDeonicaBasic(edbForm.dBasic);
                PopulateInfos();
            }*/
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite deonicu");
   
                return;
            }

            int dId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            Deonica ob = DTOManager.GetDeonica(dId);

            DeonicaEdit edbForm = new DeonicaEdit(ob);
            
            
            if (edbForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                //DTOManager.UpdateDeonicaBasic(edbForm.dBasic);
                PopulateInfos();
            }
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite deonicu");
                return;
            }

            int dId = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);

            ISession s = DataLayer.GetSession();
            Deonica d = s.Get<Deonica>(dId);
            s.Delete(d);
            s.Flush();
            s.Close();
            this.PopulateInfos();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DeonicaInformacije_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 10),
                            this.DisplayRectangle);
        }
    }
    
}
