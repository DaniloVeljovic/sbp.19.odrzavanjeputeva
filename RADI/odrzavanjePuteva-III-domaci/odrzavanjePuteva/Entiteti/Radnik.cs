﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva.Entiteti
{
    public class Radnik
    {
        public virtual int IdRadnika { get; protected set; }
        public virtual String GodinaRodjenja { get; set; }
        public virtual String Adresa { get; set; }
        public virtual String Jmbg { get; set; }
        public virtual String Ime { get; set; }
        public virtual String Prezime { get; set; }
        public virtual String OcevoIme { get; set; }
        public virtual String MatBrSefa { get; set; }
        public virtual String Specijanost { get; set; }
        public virtual DateTime DatumPostavljanja { get; set; }
        public virtual String TipRadnika { get; set; }
        public virtual IList<Upravlja> UpravljaVozilima { get; set; }
        public virtual IList<Koriste> KoristiVozila{ get; set; }
        public virtual IList<Deonica> Deonice { get; set; }
        public virtual IList<SpoljniSaradnik> SpoljniSaradnici { get; set; }

        public Radnik()
        {
            KoristiVozila = new List<Koriste>();

            UpravljaVozilima = new List<Upravlja>();

            Deonice = new List<Deonica>(); 

            SpoljniSaradnici = new List<SpoljniSaradnik>();

        }
    }
    public class Sef : Radnik
    {
        public virtual IList<Radnik> SefujeRadnik { get; set; }

        public Sef()
        {
            SefujeRadnik = new List<Radnik>();
        }

    }
    public class Izvrsilac : Radnik
    {
       
    }

    public class Nadzornik : Radnik 
    {
        
    }
   
}