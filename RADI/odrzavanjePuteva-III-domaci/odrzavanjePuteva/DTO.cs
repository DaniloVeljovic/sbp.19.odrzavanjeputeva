﻿using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace odrzavanjePuteva
{
    public class GradilistePregled
    {
        public int IdGradilista { get; protected set; }
        public String TipGradilista { get; set; }

        public GradilistePregled(int id, string tg)
        {
            this.IdGradilista = id;
            this.TipGradilista = tg;
        }
    }
    public class GradilisteBasic
    {
        public int IdGradilista { get; protected set; }
        public String TipGradilista { get; set; }

        public GradilisteBasic(int id, string tg)
        {
            this.IdGradilista = id;
            this.TipGradilista = tg;
        }
        public GradilisteBasic()
        {

        }
    }

    public class DeonicaPregled
    {
        public int IdDeonice { get; protected set; }
        public int OdKilometra { get; set; }
        public int DoKilometra { get; set; }
        public string OdGrada { get; set; }
        public string DoGrada { get; set; }
        

        public DeonicaPregled(int id, int ok, int dk, string og, string dg)
        {
            this.IdDeonice = id;
            this.OdKilometra = ok;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
        }
    }

    public class DeonicaBasic
    {
        public int deonicaId { get; set; }
        public int DoKilometra { get; set; }
        public int OdKilometra { get; set; }  //ovo sam dodala
        public DateTime DatumOd { get; set; }//ovo sam dodala
        public DateTime DatumDo { get; set; } //ovo sam dodala
        public virtual String OdGrada { get; set; }
        public virtual String DoGrada { get; set; }

        public DeonicaBasic()
        {

        }

        public DeonicaBasic(int id, int dk, string og, string dg)
        {
            this.deonicaId = id;
            this.DoKilometra = dk;
            this.OdGrada = og;
            this.DoGrada = dg;
        }
    }

    public class PutnickaPregled
    {
        public int IdVozila { get; protected set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }
        public int BrojMesta { get; set; }

        public PutnickaPregled(int id, string ro, string tg, string b, int zm, int bm)
        {
            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;
            this.BrojMesta = bm;
        }
    }

    public class VoziloPregled
    {
        public int IdVozila { get; protected set; }
        public string RegistarskaOznaka { get; set; }
        public string TipGoriva { get; set; }
        public string Boja { get; set; }
        public int ZapreminaMotora { get; set; }


        public VoziloPregled(int id, string ro, string tg, string b, int zm)
        {
            this.IdVozila = id;
            this.RegistarskaOznaka = ro;
            this.TipGoriva = tg;
            this.Boja = b;
            this.ZapreminaMotora = zm;

        }
    }

    public class IzvrsilacPregled
    {
        public int IdRadnika { get; protected set; }
        public String GodinaRodjenja { get; set; }
        public String Adresa { get; set; }
        public String Jmbg { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }

        public IzvrsilacPregled(int id, String gr, String a, String j, String i, String p)
        {
            this.IdRadnika = id;
            this.GodinaRodjenja = gr;
            this.Adresa = a;
            this.Jmbg = j;
            this.Ime = i;
            this.Prezime = p;
        }
    }

    public class IzvrsilacBasic
    {
        public int IdRadnika { get; protected set; }
        public String OcevoIme { get; set; }
        public String MatBrSefa { get; set; }
        public String Ime { get; set; }
        public String Prezime { get; set; }

        public IzvrsilacBasic()
        {
        }

        public IzvrsilacBasic(int id, String o, String mbs, String i, String p)
        {
            this.IdRadnika = id;
            this.OcevoIme = o;
            this.MatBrSefa = mbs;
            this.Ime = i;
            this.Prezime = p;
        }
    }

    public class SpoljniSaradnikPregled
    {
        public int IdSpoljnogSaradnika { get; set; }
        public int BrojUgovoraODelu { get; set; }

        public SpoljniSaradnikPregled()
        {
      
        }

        public SpoljniSaradnikPregled(int id, int buod)
        {
            this.IdSpoljnogSaradnika = id;
            this.BrojUgovoraODelu = buod;
        }
    }
}
