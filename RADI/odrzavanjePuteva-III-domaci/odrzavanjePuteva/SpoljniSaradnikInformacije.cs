﻿using NHibernate;
using odrzavanjePuteva.Entiteti;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class SpoljniSaradnikInformacije : Form
    {
        public SpoljniSaradnikInformacije()
        {
            InitializeComponent();
        }

        private void btnObrisi_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite spoljnog saradnika!");
                return;
            }
            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            SpoljniSaradnikIzmeni si = new SpoljniSaradnikIzmeni(Id);
            si.ShowDialog();
            if (si.DialogResult == DialogResult.OK)
                PopulateInfos();

         
        }

        private SpoljniSaradnik nadjiSaradnika(int id)
        {
            ISession s = DataLayer.GetSession();
            SpoljniSaradnik ss = s.Load<SpoljniSaradnik>(id);
            s.Close();
            return ss;
        }

        private void PopulateInfos()
        {
            listView1.Items.Clear();
            List<SpoljniSaradnikPregled> dInfos = DTOManager.GetSpoljniSInfo();
            foreach (SpoljniSaradnikPregled op in dInfos)
            {   
                ListViewItem item = new ListViewItem(new string[] { op.IdSpoljnogSaradnika.ToString(), op.BrojUgovoraODelu.ToString() });

                listView1.Items.Add(item);
            }
            listView1.Refresh();
        }

        private void SpoljniSaradnikInformacije_Load(object sender, EventArgs e)
        {
            this.PopulateInfos();
           listView1.Columns[0].Width = 0;
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Odaberite izvrsioca!");
                return;
            }

            int Id = Int32.Parse(listView1.SelectedItems[0].SubItems[0].Text);
            SpoljniSaradnikPregled ob = DTOManager.GetSpoljni(Id);

            DTOManager.DeleteSpoljniSaradnikPregled(ob);
            PopulateInfos();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SpoljniSaradnikDodaj ss = new SpoljniSaradnikDodaj();
            ss.ShowDialog();
            if(ss.DialogResult == DialogResult.OK)
                PopulateInfos();
        }

        private void SpoljniSaradnikInformacije_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawRectangle(new Pen(Color.White, 10),
                            this.DisplayRectangle);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
