﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace odrzavanjePuteva
{
    public partial class MessageBoxDark : Form
    {
        public MessageBoxDark()
        {
            InitializeComponent();
        }

        public String Message
        {
            get { return msg.Text; }
            set { msg.Text = value; }
        }
        public void Show(String poruka)
        {
            msg.Text = poruka;
        }
    }
}
